import { BrowserView, BrowserViewConstructorOptions, BrowserWindow, ipcMain, app } from 'electron'

export const create_preloader = (win: BrowserWindow) => {
    const view = new BrowserView({ transparent: true } as BrowserViewConstructorOptions)
    win.addBrowserView(view)
    const mainWindowBounds = win.getContentBounds()
    view.setBounds({ x: 0, y: 0, height: mainWindowBounds.height, width: mainWindowBounds.width })
    view.setBackgroundColor(`#00000000`)
    view.webContents.loadURL(app.isPackaged ? `app2://./preloader.html` : `http://localhost:${process.env.PORT}/preloader.html`)
    view.setAutoResize({ width: true, height: true })

    ipcMain.once("app-loaded", async () => {
        await view.webContents.executeJavaScript("document.getElementById('preloader-container').classList.add('preloader-hidden')");
        setTimeout(() => { win.removeBrowserView(view) }, 600)
        return;
    })
}