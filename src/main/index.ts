import { join } from 'path'
import { app, protocol, BrowserWindow, Menu } from 'electron'
import dotenv from 'dotenv'
import { createProtocol } from './protocol'
import windowStateKeeper from 'electron-window-state'
import { STATE_DATA_DIR } from '@main/core/common/computed'
import { init_ipc } from './core/ipc/all'
import { create_preloader } from './preloader'

dotenv.config({ path: join(__dirname, '../../.env') })

let win: BrowserWindow

protocol.registerSchemesAsPrivileged([{ scheme: 'app2', privileges: { secure: true, corsEnabled: true, standard: true } }])


function create_window() {
	if (app.isPackaged) {
		//Menu.setApplicationMenu(null)
	}

	const mainWindowStateKeeper = windowStateKeeper({
		defaultWidth: 1000,
		defaultHeight: 800,
		path: STATE_DATA_DIR
	})

	win = new BrowserWindow({
		transparent: false,
		frame: true,
		x: mainWindowStateKeeper.x,
		y: mainWindowStateKeeper.y,
		width: mainWindowStateKeeper.width,
		height: mainWindowStateKeeper.height,
		webPreferences: {
			nodeIntegration: false,
			contextIsolation: true,
			webviewTag: false,
			webSecurity: true,
			preload: join(__dirname, '../preload/index.js'),
			devTools: true,
		},
		title: "Baking Buddy",
		backgroundColor: "#191919",
		show: false,
		icon: __dirname + "public/icon.png"
	})

	const URL = app.isPackaged
		? `app2://./index.html`
		: `http://localhost:${process.env.PORT}`

	win.once("ready-to-show", () => {
		win.show()
		win.focus()
	})
	//create_preloader(win)
	init_ipc()

	win?.loadURL(URL)

}

const load_dev_tools = async () => {
	//if (app.isPackaged) return;
	try {
		const { default: installExtension, VUEJS3_DEVTOOLS } = await import('electron-devtools-installer');
		await installExtension(VUEJS3_DEVTOOLS, {
			loadExtensionOptions: {
				allowFileAccess: true,
			},
			forceDownload: false
		})
	} catch (e) {
		console.error('Vue Devtools failed to install:', e.toString())
	}
}


app.whenReady()
	.then(() => createProtocol('app2'))
	.then(load_dev_tools)
	.then(create_window)

app.on('window-all-closed', () => {
	if (process.platform !== 'darwin') {
		app.quit()
	}
})