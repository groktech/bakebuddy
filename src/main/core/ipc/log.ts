import { ipcMain } from 'electron'
import { use_logs } from '../common/log';

const create_ipc_log_method = (level: string, fn: Function) => { 
	ipcMain.on(level, (event, id, msg) => {
		try {
			fn(msg)
			event.reply(id, { success: true })
        } catch (err) {
            event.reply(id, { success: false, error: err })
        }
	});
}

export const init_log_ipc = async () => {
	const { trace, debug, info, warn, error, fatal } = use_logs()
	create_ipc_log_method("trace", trace)
	create_ipc_log_method("debug", debug)
	create_ipc_log_method("info", info)
	create_ipc_log_method("warn", warn)
	create_ipc_log_method("error", error)
	create_ipc_log_method("fatal", fatal)
}