import { homedir, tmpdir } from 'os'
import { ipcMain } from 'electron'
import open from 'open'

export const init_os_ipc = () => {
    ipcMain.on(`homedir`, (event, id) => {
        try {
            event.reply(id, { success: true, result: homedir() })
        } catch (err) {
            event.reply(id, { success: false, error: err })
        }
    })

    ipcMain.on(`tmpdir`, async (event, id) => {
        try {
            const result = tmpdir()
            event.reply(id, { success: true, result })
        } catch (err) {
            event.reply(id, { success: false, error: err })
        }
    })

    ipcMain.on(`get_env`, (event, id, { name }: { name: string }) => {
        try {
            event.reply(id, { success: true, result: process.env[name] })
        } catch (err) {
            event.reply(id, { success: false, error: err })
        }
    })

    ipcMain.on(`cwd`, (event, id) => {
        try {
            event.reply(id, { success: true, result: process.cwd() })
        } catch (err) {
            event.reply(id, { success: false, error: err })
        }
    })

    ipcMain.on(`open`, async (event, id, { url }: { url: string }) => {
        try {
            await open(url)
            event.reply(id, { success: true })
        } catch (err) {
            event.reply(id, { success: false, error: err })
        }
    })   
}