import { init_net_ipc } from './net'
import { init_fs_ipc } from './fs'
import { init_os_ipc } from './os'
import { init_clipboard_ipc } from './clipboard'
import { init_log_ipc } from './log'
import { init_proc_ipc } from './proc'

export const init_ipc = async () => {
    init_fs_ipc()
    init_net_ipc()
    init_os_ipc()
    init_clipboard_ipc()
    init_proc_ipc()
	await init_log_ipc()
}