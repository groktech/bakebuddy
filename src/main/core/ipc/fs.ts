import { ipcMain } from 'electron'
import { readdir } from 'fs-extra'

import { writeFile, rename, access, readFile, mkdir, realpath, stat } from 'fs/promises'
import { sha256sum, sha256sum_data } from '@main/core/common/hashes'
import { join, dirname, basename } from "path"
import { AtomicWritableOptions } from '@src/common/interfaces/fs'
import { lock } from '../common/critical'

function getTempFile(file: string) {
    return join(dirname(file), '.~' + basename(file))
}

export const init_fs_ipc = () => {
    ipcMain.on(`readFile`, async (event, id, { path, options }: { path: string, options: object }) => {
        try {
            const data = await readFile(path, options)
            event.reply(id, { success: true, data });
        } catch (err) {
            event.reply(id, { success: false, error: err })
        }
    })

    ipcMain.on(`readdir`, (event, id, { path }) => {
        try {
            readdir(path, (err, files) => {
                event.reply(id, { success: !err, error: err, result: files })
            })
        } catch (err) {
            event.reply(id, { success: false, error: err })
        }
    })

    ipcMain.on(`mkdirp`, async (event, id, { path }) => {
        try {
            await mkdir(path, { recursive: true })
            event.reply(id, { success: true })
        } catch (err) {
            event.reply(id, { success: false, error: err })
        }
    })
    ipcMain.on(`realpath`, async (event, id, { path, options }) => {
        try {
            const result = await realpath(path, options)
            event.reply(id, { success: true, result })
        } catch (err) {
            event.reply(id, { success: false, error: err })
        }
    })

    ipcMain.on(`writeFile`, async (event, id, { path, data, options }: { path: string, data: any, options: AtomicWritableOptions | string }) => {
        let unlock: () => void = () => { }
        try {
            let targetPath = path
            if (typeof options !== "string" && typeof options === 'object' && options.atomic) {
                unlock = await lock(`writeFile${targetPath}`)
                path = getTempFile(path)
            }
            await writeFile(path, data, options as any)
            if (typeof options !== "string" && typeof options === 'object' && options.atomic) {
                await rename(path, targetPath)
            }
            event.reply(id, { success: true })
        } catch (err) {
            console.error(err)
            event.reply(id, { success: false, error: err })
        } finally {
            unlock()
        }
    })
    ipcMain.on(`rename`, async (event, id, { oldPath, newPath }: { oldPath: string, newPath: string }) => {
        try {
            await rename(oldPath, newPath)
            event.reply(id, { success: true })
        } catch (err) {
            event.reply(id, { success: false, error: err })
        }
    })
    ipcMain.on(`exists`, async (event, id, { path }: { path: string }) => {
        try {
            await access(path)
            event.reply(id, { success: true })
        } catch (err) {
            event.reply(id, { success: false })
        }
    })

    ipcMain.on('statIsFile', async (event, id, { path }: { path: string }) => {
        try {
            const stats = await stat(path)
            event.reply(id, { success: true, result: stats?.isFile() })
        } catch (err) {
            event.reply(id, { success: false, error: err })
        }
    })

    ipcMain.on('statIsDirectory', async (event, id, { path }: { path: string }) => {
        try {
            const stats = await stat(path)
            event.reply(id, { success: true, result: stats?.isDirectory() })
        } catch (err) {
            event.reply(id, { success: false, error: err })
        }
    })

    ipcMain.on('sha256sum_file', async (event, id, { path }: { path: string }) => {
        try {
            const result = await sha256sum(path)
            event.reply(id, { success: true, result: result })
        } catch (err) {
            event.reply(id, { success: false, error: err })
        }
    })

    ipcMain.on('sha256sum_data', async (event, id, { data }: { data: Buffer }) => {
        try {
            const result = sha256sum_data(data)
            event.reply(id, { success: true, result })
        } catch (err) {
            event.reply(id, { success: false, error: err })
        }
    })
}