import { join } from 'path'
import { STATE_DATA_DIR } from '@main/core/common/computed'

export const _resolve_path = (path: string) => {
    const root = STATE_DATA_DIR
    const result = join(root, path)
    console.log(result)
    if (!result.startsWith(root)) throw new Error("forbidden")
    return result
}