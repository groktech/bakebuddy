import { promisify } from "util"
import { spawn, exec as execCb } from "child_process"
const exec = promisify(execCb);
import { ipcMain } from 'electron'
import { IExecOptions } from "./interfaces";
import { exec as sudo_exec } from "sudo-prompt"
import { use_logs } from "../common/log"

export const init_proc_ipc = () => {
	ipcMain.on(`exec`, async (event, id, { cmd, options }: { cmd: string, options: any }) => {
		try {
			event.reply(id, { success: true, result: await exec(cmd, options) })
		} catch (err) {
			event.reply(id, { success: false, error: err })
		}
	})
	ipcMain.on(`spawn`, async (event, id, { bin, args, options }: { bin: string, args: Array<string>, options: IExecOptions }) => {
		try {
			console.log("spawn", bin, args)
			const _stdout = (data: any) => {
				event.reply(`${id}_stdout`, data)
			}
			const _stderr = (data: any) => {
				event.reply(`${id}_stderr`, data)
			}

			const proc = spawn(bin, args);
			proc.stdout.on(`data`, _stdout)
			proc.stderr.on(`data`, _stderr)
			proc.once(`exit`, (code) => {
				proc.stdout.removeAllListeners()
				proc.stderr.removeAllListeners()
				event.reply(`${id}_exit`, code)
			})

			event.reply(id, { success: true })
		} catch (err) {
			event.reply(id, { success: false, error: err })
		}
	})
	ipcMain.on(`sudo`, async (event, id, cmd) => {
		const { error } = use_logs()
		try {
			sudo_exec(cmd, { name: "Bake Buddy" }, (err, stdout, stderr) => {
				if (err) {
					error(err.message)
					event.reply(id, { success: false, error: err })
					return
				}
				event.reply(id, { success: true, stdout, stderr })
			})
		} catch (err) {
			error(err.message)
			event.reply(id, { success: false, error: err })
		}
	})
}