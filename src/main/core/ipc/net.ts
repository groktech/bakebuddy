import { ipcMain } from 'electron'
import { download_file, download_string } from '@main/core/common/net'
import { _resolve_path } from './util'
import { isIP } from 'net'

export const init_net_ipc = () => {

    ipcMain.on(`download_string`, async (event, id, { sourceURL }: { sourceURL: string }) => {
        try {
            const result = await download_string(sourceURL)
            event.reply(id, { success: true, data: result })
        } catch (err) {
            event.reply(id, { success: false, error: err })
        }
    })

    ipcMain.on(`download_file`, async (event, id, { sourceURL, targetPath }: { sourceURL: string, targetPath: string }) => {
        try {
            await download_file(sourceURL, _resolve_path(targetPath))
            event.reply(id, { success: true })
        } catch (err) {
            event.reply(id, { success: false, error: err })
        }
    })
    ipcMain.on(`is_ip`, async (event, id, { ip }: { ip: string }) => {
        try {
            event.reply(id, { success: true, result: isIP(ip) })
        } catch (err) {
            event.reply(id, { success: false, error: err })
        }
    })
}