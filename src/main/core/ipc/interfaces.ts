export interface IExecOptions {
    exit?: (code?: number) => void,
    stdout?: (data: Buffer | ArrayBuffer | Uint8Array) => void,
    stderr?: (data: Buffer | ArrayBuffer | Uint8Array) => void
}