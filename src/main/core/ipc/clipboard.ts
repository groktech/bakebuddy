import { clipboard, ipcMain } from 'electron'

export const init_clipboard_ipc = () => {
    ipcMain.on(`write_to_clipboard`, async (event, id, { data }: { data: string }) => {
        try {
            clipboard.writeText(data);
            event.reply(id, { success: true })
        } catch (err) {
            event.reply(id, { success: false, error: err })
        }
    })
}