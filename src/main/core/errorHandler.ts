import { use_logs } from '@main/core/common/log'

process.on("uncaughtException", (err) => {
	console.log(err)
	const { fatal } = use_logs()
	fatal(err)
	process.exit(1)
})