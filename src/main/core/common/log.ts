import pino from "pino"
import { accessSync, mkdirSync } from "fs"
import { join } from "path"

import { LOG_DIR } from '@main/core/common/computed'
import { BB_VERSION } from "@src/common/constants"


const logLevel = process.env.NODE_ENV === "development" ? "trace" : "info"

export const init_log = () => {
	try {
		accessSync(LOG_DIR)
	} catch {
		mkdirSync(LOG_DIR, { recursive: true })
	}
	return pino({
		level: logLevel,
		base: null,
		formatters: {
			level: (label) => ({ level: label.toUpperCase() })
		},
		customLevels: {
			success: 31
		},
		name: `Bake Buddy ${BB_VERSION}`,
	}, pino.destination({ dest: join(LOG_DIR, "bake-buddy.log"), sync: false }))
}

let _logger: pino.Logger | undefined = undefined
export const use_logs = () => {
	if (_logger === undefined) {
		_logger = init_log()
	}

	return {
		trace: (msg: string | object) => typeof msg === 'object' ? _logger?.trace(msg) : _logger?.trace(msg),
		debug: (msg: string | object) => typeof msg === 'object' ? _logger?.debug(msg) : _logger?.debug(msg),
		info: (msg: string | object) => typeof msg === 'object' ? _logger?.info(msg) : _logger?.info(msg),
		warn: (msg: string | object) => typeof msg === 'object' ? _logger?.warn(msg) : _logger?.warn(msg),
		error: (msg: string | object) => typeof msg === 'object' ? _logger?.error(msg) : _logger?.error(msg),
		fatal: (msg: string | object) => typeof msg === 'object' ? _logger?.fatal(msg) : _logger?.fatal(msg),
	}
}
