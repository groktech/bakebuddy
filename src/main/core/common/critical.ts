const locks: { [key: string]: { locked: boolean, waiters: Array<() => void> } } = {}

const _direct_lock = (name: string) => {
    if (locks[name]) {
        locks[name].locked = true
    } else {
        locks[name] = { locked: true, waiters: [] }
    }
}

const _direct_unlock = (name: string) => {
    if (locks[name]) {
        if (locks[name].waiters.length > 0) {
            const _next_lock = locks[name].waiters.splice(0, 1);
            _next_lock[0]()
        } else {
            delete locks[name]
        }
    }
}

export const lock = async (name: string): Promise<() => void> => {
    if (locks[name]?.locked) {
        return new Promise((resolve, reject) => {
            locks[name].waiters.push(() => {
                _direct_lock(name)
                resolve(() => _direct_unlock(name))
            })
        })
    } else {
        _direct_lock(name)
    }
    return () => _direct_unlock(name)
}

export const critical_section = async (name: string, todo: Function) => {
    const _unlock = await lock(name);
    try {
        todo();
    }
    catch (err) {
        throw err;
    } finally {
        _unlock()
    }
}