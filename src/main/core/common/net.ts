import axios from "axios"
import { ensureDirSync, createWriteStream } from 'fs-extra'
import { dirname } from 'path'
import { Stream } from 'stream'
import { BB_VERSION} from '@src/common/constants'
import { use_logs } from "./log"

export const download_string = async (sourceURL: string): Promise<any> => {
    const response = await axios.get(sourceURL, {
        headers: { 'User-Agent': `Bake Buddy ${BB_VERSION}` },
        transformResponse: res => res
    })
    return response.data
}

export const download_file = async (sourceURL: string, targetPath: string) => {
    ensureDirSync(dirname(targetPath));
	const { trace } = use_logs()
    trace(`Downloading ${sourceURL} to ${targetPath}`);
    const response = await axios({
        method: "get",
        url: sourceURL,
        responseType: "stream",
        headers: { 'User-Agent': `Bake Buddy ${BB_VERSION}` }
    })
    await new Promise((resolve, reject) => {
        try {
            const stream = (response.data as Stream)
            stream.once("error", (err: Error) => {
                stream.removeAllListeners()
                reject(err)
            })
            stream.once("end", () => {
                resolve(true)
            })
            stream.pipe(createWriteStream(targetPath))
        }
        catch (err) {
            reject(err)
        }
    })
}