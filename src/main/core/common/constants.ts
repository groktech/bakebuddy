let _dataDirCandidate = undefined
for (const arg of process.argv) {
	let _match = arg.match(/--data-dir=(?<datadir>.*)/)
	_match = _match || arg.match(/-dd=(?<datadir>.*)/)
	if (_match) {
		_dataDirCandidate = _match.groups?.datadir
	}
} 

export const DATA_DIR_NAME = _dataDirCandidate ?? ".bake-buddy"
