import { createReadStream } from 'fs-extra'
import { createHash } from 'crypto'

export const sha256sum = async (path: string): Promise<string> => {
    return new Promise((resolve, reject) => {
        try {
            const hash = createHash('sha256');
            const stream = createReadStream(path);
            stream.pipe(hash)
            stream.on('end', function () {
                const result = hash.digest('hex').toUpperCase()
                resolve(result);
            });
        } catch (err) {
            reject(err);
        }
    });
}

export const sha256sum_data = (data: Buffer | Uint8Array) => {
    const hash = createHash('sha256');
    hash.update(data)
    return hash.digest('hex').toUpperCase()
}