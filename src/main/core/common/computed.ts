import { join } from "path";
import { DATA_DIR_NAME } from "./constants";
import {app} from 'electron';

export const STATE_DATA_DIR = join(app.getPath('home'), DATA_DIR_NAME)
export const LOG_DIR = join(STATE_DATA_DIR, "logs")