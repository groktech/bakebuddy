import { WritableOptions } from "stream";

export interface AtomicWritableOptions extends WritableOptions {
    atomic?: boolean
}