export const format_time_to_ago = (time: number, currentTime?: number) => {
    const _currentTime = currentTime ?? new Date().getTime()
    const difference = _currentTime - time
    if (difference < 60000) {
        return "just now"
    }
    if (difference < 60 * 60000) {
        return `${Math.floor(difference / 60000)} min. ago`
    }
    if (difference < 24 * 60 * 60000) {
        return `${Math.floor(difference / (60000 * 60))} h. ago`
    }
    if (difference < 24 * 60 * 60000 * 7) {
        return `${Math.floor(difference / (60000 * 60 * 24))} day ago`
    }
    const weeks = Math.floor(difference / (60000 * 60 * 24 * 7));
    return weeks > 1 ? `${weeks} weeks ago` : `${weeks} week ago`
}