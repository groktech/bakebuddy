import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'

import Home from "@render/views/Home.vue"
import About from '@render/views/About.vue'
import Log from '@render/views/Log.vue'
import Setup from '@render/views/Setup.vue'
import NotFound from '@render/views/NotFound.vue'
import { computed } from 'vue'

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/setup',
    name: 'Setup',
    component: Setup
  },
  {
    path: '/about',
    name: 'About',
    component: About
  },
  {
    path: '/logs',
    name: 'Log',
    component: Log
  },
  { path: '/:pathMatch(.*)*', redirect: '/not-found' },
  { path: '/:pathMatch(.*)', redirect: '/not-found' },
  {
    path: '/not-found',
    name: "NotFound",
    component: NotFound
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes
})


export const currentRoute = computed(() => {
  return router.currentRoute.value
})

export const currentRouteParams = computed(() => {
  return currentRoute.value.params
})

export function get_current_route_param(id: string, single: true): string
export function get_current_route_param(id: string, single: false): string|string[] 
export function get_current_route_param(id: string, single: boolean = false) {
  const param = currentRouteParams.value[id]
  if (Array.isArray(param) && single) {
    return param[0]
  }
  return param
}

export default router
