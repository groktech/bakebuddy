export default {
    main: [
        { link: "/", icon: "home" },
        { text: "Journal", link: "/journal", icon: "book-alt" },
    ],
    footer: [
        { text: "Settings", link: "/settings", icon: "cog" },
        {
          text: "Support The Project",
          link: "/support-the-project",
          icon: "seedling"
        },
        { text: "About", link: "/about", icon: "info" }
    ]
}