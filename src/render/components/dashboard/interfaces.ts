export interface IDashboardContextBindOptions {
    columnSpan?: number,
    rowSpan?: number,
    index?: number,
    positionRight?: boolean,
    shrinkable?: number,
    name: string
    adjust_style: (style: { [key: string]: string }) => void
}

export interface IDashboardContextHooks {
    bind: (uid: string, options: IDashboardContextBindOptions) => void
    unbind: (uid: string) => void,
    request_recalculate: () => void
}