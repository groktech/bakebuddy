import { warn } from "@render/common/log";
import { getCurrentInstance, inject, onUnmounted } from "vue";

export interface IValidationContextHooks {
  bind: (uid: number, validate: () => Promise<boolean> | boolean) => void
  unbind: (uid: number) => void
}

export class ValidationContext implements IValidationContextHooks {
  private validators: { [key: string]: () => Promise<boolean> | boolean } = {}

  bind(uid: number, validate: () => Promise<boolean> | boolean) {
    this.validators[uid] = validate
  }
  unbind(uid: number) {
    delete this.validators[uid]
  }
  async validate() {
    for (const validate of Object.values(this.validators)) {
      if (typeof validate === "function" && !(await validate())) {
        return false;
      }
    }
    return true;
  }
}

export const create_validation_context = () => {
  return new ValidationContext()
}

export const join_validation_context = (validate: () => Promise<boolean> | boolean) => {
  if (getCurrentInstance()?.parent?.exposed?.validator === true) {
    const validationContext = inject("validation") as IValidationContextHooks
    if (validationContext instanceof ValidationContext) {
      const uid = getCurrentInstance()?.uid ?? -1
      validationContext.bind(uid, validate)
      onUnmounted(() => validationContext.unbind(uid))
    } else {
      warn("Failed to join validation context!")
    }
  }
}