export const trace = window.log.trace
export const debug = window.log.debug
export const info = window.log.info
export const warn = window.log.warn
export const error = window.log.error
export const fatal = window.log.fatal
