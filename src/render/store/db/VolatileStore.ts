import { MutationTree, ActionTree, GetterTree, Module } from 'vuex';

abstract class VolatileStore<S, R, M, A, G> implements Module<S, R> {
    abstract state: S
    abstract mutations: MutationTree<S> & M
    abstract actions: ActionTree<S, R> & A
    abstract getters: GetterTree<S, R> & G

    cleanup() {}
}

export default VolatileStore