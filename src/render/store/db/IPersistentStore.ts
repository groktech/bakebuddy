import PersistanceFiltrationMode from './PersistanceFiltrationMode';

interface IPersistantStore {
    mode: PersistanceFiltrationMode
    getFilter: () => Array<string>
    saveState: (cache?: boolean) => void
    loadState:() => void
}

export default IPersistantStore