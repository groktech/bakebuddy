import PersistanceFiltrationMode from './PersistanceFiltrationMode';
import { MutationTree, ActionTree, GetterTree, Module } from 'vuex';
import FileAdapter from './fs/FileAdapter';
import IPersistantStore from './IPersistentStore';
import { debug } from '@render/common/log';
import { join } from '@render/core/common/path';
import { homedir } from '@render/core/ipc/os';

abstract class PersistantStore<S, R, M, A, G> implements Module<S, R>, IPersistantStore {
    abstract state: S
    abstract defaultState: S
    public mode: PersistanceFiltrationMode = PersistanceFiltrationMode.WHITE_LIST
    private adapter?: FileAdapter<S> = undefined
    private sourceFile: string
    private _filter?: Array<string>

    constructor(sourceFile: string) {
        this.sourceFile = sourceFile
        this.loadState = this.loadState.bind(this)
        this.saveState = this.saveState.bind(this)
    }

    abstract preprocess_state(data: any): S

    getFilter() {
        if (this._filter === undefined) {
            this._filter = Object.keys(this.mutations)
        }
        return this._filter
    }

    private async get_adapter() {
        if (this.adapter === undefined) {
            this.adapter = await FileAdapter.init(join(await homedir(), ".bake-buddy", this.sourceFile), {
                defaultValue: this.defaultState
            })
        }
        return this.adapter
    }

    async saveState(cache?: boolean) {
        await (await this.get_adapter()).write(this.state, cache);
        debug("state saved")
    }

    async loadState(strict: boolean = true) {
        let data = this.defaultState
        try {
            data = await (await this.get_adapter()).read() ?? this.defaultState
        } catch (err) {
            if (strict) throw err;
        }
        this.state = data as S
    }

    abstract mutations: MutationTree<S> & M
    abstract actions: ActionTree<S, R> & A
    abstract getters: GetterTree<S, R> & G
}

export default PersistantStore