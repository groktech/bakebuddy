enum PersistanceFiltrationMode {
    WHITE_LIST,
    BLACK_LIST
}

export default PersistanceFiltrationMode