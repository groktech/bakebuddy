export type DeserializeStringFunc = (serializedData: string) => any
export type SerializeStringFunc = (serializedData: any) => string

interface IAdapterOptions<SchemaT = any> {
    defaultValue?: SchemaT;
    serialize?: SerializeStringFunc;
    deserialize?: DeserializeStringFunc;
}

export default IAdapterOptions