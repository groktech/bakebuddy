import IAdapterOptions, { SerializeStringFunc, DeserializeStringFunc } from './interfaces/IAdapterOptions'

function stringify(obj: any) {
    return JSON.stringify(obj);
}

function parse(obj: string) {
    return JSON.parse(obj)
}
/**
 * @type {Class<SchemaT>}
 */
class BaseAdapter<SchemaT> {
    source: string
    serialize: SerializeStringFunc
    deserialize: DeserializeStringFunc
    defaultValue: SchemaT

    constructor(source: string, options: IAdapterOptions) {
        this.source = source;
        this.serialize = options && options.serialize ? options.serialize : stringify;
        this.deserialize = options && options.deserialize ? options.deserialize : parse;
        this.defaultValue = options && options.defaultValue !== undefined ? options.defaultValue : {}
    }
}

export { BaseAdapter }