import { writeFile, rename } from '@render/core/ipc/fs'
import { sha256sum } from '@render/core/common/crypto'

const writers: { [key: string]: SafeWriter } = {}

class DataToWrite {
    data: string
    cache?: boolean
    constructor(data: any, cache?: boolean) {
        this.data = data;
        this.cache = cache
    }
}

class SafeWriter {
    toProcess: Array<DataToWrite> = []
    file: string
    lock: boolean = false
    private lastWritten?: string = undefined

    constructor(file: string) {
        this.file = file
    }

    async processWrite(dataToWrite: DataToWrite) {
        if (this.lock) {
            this.toProcess.push(dataToWrite)
            return
        }

        try {
            const _encoder = new TextEncoder()
            const newHash = await sha256sum(_encoder.encode(dataToWrite.data), 'hex')
            if (!dataToWrite.cache || newHash !== this.lastWritten) {
                this.lock = true
                await writeFile(this.file, dataToWrite.data, { atomic: true })
                this.lastWritten = newHash
            }
        }  finally {
            this.lock = false
            const newDataToWrite = this.toProcess.shift()
            if (newDataToWrite) {
                await this.processWrite(newDataToWrite)
            }
        }
    }

    async write(data: string, cache?: boolean) {
        await this.processWrite(new DataToWrite(data, cache))
    }
}

export const write = async function (file: string, data: string, cache?: boolean) {
    if (!data) return
    writers[file] = writers[file] ?? new SafeWriter(file)
    await writers[file].write(data, cache)
}