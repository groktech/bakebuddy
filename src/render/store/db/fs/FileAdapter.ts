import { write as writeFile } from './SafeWriter'
import { BaseAdapter } from './BaseAdapter'
import { dirname } from '@render/core/common/path'
import IAdapterOptions from './interfaces/IAdapterOptions'
import { readFile, mkdirp } from '@render/core/ipc/fs'

class FileAdapter<SchemaT> extends BaseAdapter<SchemaT> {
    static async init<SchemaT>(source: string, options: IAdapterOptions) {
        await mkdirp(dirname(source))
        return new FileAdapter<SchemaT>(source, options)
    }

    private constructor(source: string, options: IAdapterOptions) {
        super(source, options);
    }

    async write(data: SchemaT, cache?: boolean) {
        await writeFile(this.source, this.serialize(data), cache);
    }

    /** Loads database from file */
    async read(): Promise<SchemaT> {
        const data = await readFile(this.source);
        const _decoder = new TextDecoder();
        return data ? await this.deserialize(_decoder.decode(data)) : this.defaultValue;
    }
}

export default FileAdapter