import { Store } from 'vuex'
import PersistanceFiltrationMode from './PersistanceFiltrationMode'
import IPersistantStore from './IPersistentStore'

class PersistantStoresPlugin<T> {
    name: string
    modules: { [key: string]: IPersistantStore } = {}

    constructor(name?: string) {
        this.name = name || Math.random().toString(36).substring(7)
    }

    registerModule(id: string, persistantStore: IPersistantStore) {
        this.modules[id] = persistantStore
    }

    unregisterModule(id: string) {
        delete this.modules[id]
    }

    hasModule(id:string) {
        return !!this.modules[id]
    }

    get plugin() {
        return (store: Store<any>) => {
            store.subscribe((async (mutation, state) => {
                for (const m of Object.values(this.modules)) {
                    if (this.shouldPersist(mutation.type, m.getFilter(), m.mode)) {
                        await m.saveState(true)
                    }
                }
            }))
        }
    }

    shouldPersist(type: string, filter?: Array<string>, mode?: PersistanceFiltrationMode) {
        switch (mode ?? PersistanceFiltrationMode.BLACK_LIST) {
            case PersistanceFiltrationMode.WHITE_LIST: return filter?.includes(type) ?? true;
            case PersistanceFiltrationMode.BLACK_LIST: return !filter?.includes(type) ?? false;
        }
    }
}

export default PersistantStoresPlugin