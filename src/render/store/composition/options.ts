import store from "@render/store/store"
import { computed } from "@vue/runtime-core"
import { EConfigurationOptions } from "@render/store/sub-stores/configuration/store/interfaces"

export const create_configurable_options = (...options: EConfigurationOptions[]) => {
    const result = []
    for (const name of options) {
        result.push(computed({
            get: () => store.Getters.Option(name),
            set: (value: any) => store.Actions.setConfigurationOption({ name, value })
        }));
    }
    return result;
}

export const get_option_values = (...options: EConfigurationOptions[]) => {
    const result = []
    for (const name of options) {
        result.push(store.Getters.Option(name));
    }
    return result;
}