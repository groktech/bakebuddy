import { StoreOptions, Store, Module, ModuleOptions } from 'vuex'

// TODO: migrate to create sotre

import AppStore from './sub-stores/account/store/AccountStore'
import { DirectActions, IStoreActionsList, DirectGetters, IStoreGetterList, DirectMutations, IStoreMutationList, AllGetters, AllActions, AllMutations } from './interfaces/Types'
import { get, set, clone, unset } from 'lodash'

export const accountStore = new AppStore()

const storeOptions = {
  state: {
    ...accountStore.defaultState
  },
  mutations: {
    ...accountStore.mutations
  },
  actions: {
    ...accountStore.actions
  },
  getters: {
    ...accountStore.getters
  },
  plugins: [
    ...accountStore.plugins
  ]
}

class TypedStore<O, S, G, A, M> extends Store<S> {
  Actions: DirectActions<O & A> & { [key: string]: any }
  Mutations: DirectMutations<O & M> & { [key: string]: any }
  Getters: DirectGetters<O & G> & { [key: string]: any }
  constructor(options: O & StoreOptions<S>) {
    super(options)

    this.Actions = this.getActions(options.actions as IStoreActionsList) as DirectActions<O & A> & { [key: string]: any }
    this.Getters = this.getGetters(options.getters as IStoreGetterList) as DirectGetters<O & G> & { [key: string]: any }
    this.Mutations = this.getMutations(options.mutations as IStoreMutationList) as DirectMutations<O & M> & { [key: string]: any }
  }

  getActions(source: IStoreActionsList): DirectActions<O> {
    const actions: { [key: string]: any } = {}
    for (const name of Object.keys(source as IStoreActionsList)) {
      actions[name] = (payload?: any) => this.dispatch(`${name}`, payload)
    }
    return actions as DirectActions<O>
  }

  getGetters(source: IStoreGetterList): DirectGetters<O> {
    const getters: { [key: string]: (state: S) => any } = {}
    for (const name of Object.keys(source)) {
      //const getter = Object.getOwnPropertyDescriptor(this.getters, name)
      //Object.defineProperty(getters, name, getter)
      Object.defineProperties(getters, {
        [name]: {
          get: () => {
            return this.getters[name]
          }
        }
      })
    }
    return getters as DirectGetters<O>
  }

  getMutations(source: IStoreMutationList): DirectMutations<O> {
    const mutations: { [key: string]: any } = {}
    for (const name of Object.keys(source as IStoreMutationList)) {
      mutations[name] = (payload?: any) => this.commit(`${name}`, payload)
    }
    return mutations as DirectMutations<O>
  }

  addModuleActions(path: string | string[], source: IStoreActionsList) {
    let actions = this.getActions(source as IStoreActionsList)
    set(this.Actions, path, actions)
    actions = clone(actions)
    Object.assign(actions, this.Actions)
    this.Actions = actions as DirectActions<O & A>
  }

  removeModuleActions(path: string | string[]) {
    const actions = get(this.Actions, path)
    if (!actions) return
    for (const name of Object.keys(actions as IStoreActionsList)) {
      if (this.Actions[name] === actions[name]) {
        delete this.Actions[name]
      }
    }
    unset(this.Actions, path)
  }

  addModuleMutations(path: string | string[], source: IStoreMutationList) {
    let mutations = this.getMutations(source)
    set(this.Mutations, path, mutations)
    mutations = clone(mutations)
    Object.assign(mutations, this.Mutations)
    this.Mutations = mutations as DirectMutations<O & M>
  }

  removeModuleMutations(path: string | string[]) {
    const mutations = get(this.Actions, path) as IStoreMutationList
    if (!mutations) return
    for (const name of Object.keys(mutations)) {
      if (this.Mutations[name] === mutations[name]) {
        delete this.Mutations[name]
      }
    }
    unset(this.Mutations, path)
  }

  addModuleGetters(path: string | string[], source: IStoreGetterList) {
    const getters = this.getGetters(source)
    set(this.Getters, path, getters)
    for (const name of Object.keys(source)) {
      if (name in this.Getters)
        continue;

      Object.defineProperties(this.Getters, {
        [name]: {
          get: () => {
            return this.getters[name]
          }
        }
      })
    }
  }

  removeModuleGetters(path: string | string[]) {
    const getters = get(this.Getters, path) as IStoreGetterList
    if (!getters) return
    for (const name of Object.keys(getters)) {
      if (this.Getters[name] === getters[name]) {
        delete this.Getters[name]
      }
    }
    unset(this.Getters, path)
  }


  registerModule<T>(path: string | string[], module: Module<T, S>, options?: ModuleOptions) {
    if (Array.isArray(path)) {
      super.registerModule(path, module, options)
    } else {
      super.registerModule(path, module, options)
    }
    this.addModuleActions(path, module.actions as IStoreActionsList)
    this.addModuleMutations(path, module.mutations as IStoreMutationList)
    this.addModuleGetters(path, module.getters as IStoreGetterList)
  }

  unregisterModule(path: string | string[]) {
    if (Array.isArray(path)) {
      super.unregisterModule(path)
    } else {
      super.unregisterModule(path)
    }
    this.removeModuleActions(path)
    this.removeModuleMutations(path)
    this.removeModuleGetters(path)
  }

}
const store = new TypedStore<typeof storeOptions, any, { getters: AllGetters }, { actions: AllActions }, { mutations: AllMutations }>(storeOptions)

export default store