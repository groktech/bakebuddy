import { INotificationContext, ENotificationType } from "./store/interfaces";
import { v4 as uuidv4 } from 'uuid';
import store from '@render/store/store';
import { EConfigurationOptions } from "../configuration/store/interfaces";

class NotificationContext implements INotificationContext {
    id: string;
    source: string;
    text: string;
    hint: string;
    type: ENotificationType;

    constructor(source: string, text: string, type: ENotificationType, hint?: string, toBeClosed: boolean = true) {
        this.id = uuidv4()
        this.source = source
        this.text = text;
        this.type = type;
        this.hint = hint ?? "";
        if (toBeClosed) {
            setTimeout(() => {
                this.close()
            }, store.Getters.Option(EConfigurationOptions.NotificationTimeout) ?? 10000)
        }
    }

    close() {
        if (typeof store.Mutations.removeNotification === 'function') {
            store.Mutations.removeNotification(this.id)
        }
    }

}

export const close_notification = (id: string) => {
    if (typeof store.Mutations.removeNotification === 'function') {
        store.Mutations.removeNotification(id)
    }
}

export default NotificationContext