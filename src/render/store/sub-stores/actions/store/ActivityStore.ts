import { GetterTree, ActionTree, MutationTree } from 'vuex'
import { IActivityStoreData, IActivityStoreMutations, IActivityStoreActions, IActivityStoreGetters } from './interfaces'
import IGlobalStateData from '../../IGlobalStateData'
import { findIndex } from 'lodash'
import NotificationContext from '../NotificationContext'
import store from '@render/store/store'
import { EConfigurationOptions } from '../../configuration/store/interfaces'
import PersistentStore from '@render/store/db/PersistentStore'

class ActivityStore extends PersistentStore<IActivityStoreData, IGlobalStateData, IActivityStoreMutations, IActivityStoreActions, IActivityStoreGetters> {
    constructor(sourceFile: string) {
        super(sourceFile);
    }
    get defaultState() { return ActivityStore.defaultState; }
    static get defaultState() {
        const result: IActivityStoreData = {
            notifications: [
                //new NotificationContext("test", "test1", ENotificationType.success, "test1 hint")
            ],
            journal: [],
            log: []
        }
        return result;
    }

    state = this.defaultState

    mutations: MutationTree<IActivityStoreData> & IActivityStoreMutations = {
        notify(state, notification) {
            state.notifications.splice(0, 0, notification)
            if (state.notifications.length > store.Getters.Option(EConfigurationOptions.NotificationCountLimit)) {
                state.notifications.splice(store.Getters.Option(EConfigurationOptions.NotificationCountLimit))
            }
            state.journal.splice(0, 0, { dateTime: new Date().toLocaleString(), type: notification.type, source: notification.source, text: notification.text })
            state.journal.splice(store.Getters.Option(EConfigurationOptions.JournalCapacity) ?? 999)
        },
        addLog (state, message) {
            if (!Array.isArray(state.log)) {
                state.log = []
            }
            state.log.push(message)
        },
        removeNotification(state, id) {
            const notificationIndex = findIndex(state.notifications, { id: id })
            if (notificationIndex >= 0) {
                state.notifications.splice(notificationIndex, 1);
            }
        },
        clearLog(state) {
            state.log.splice(0)
        },
        clearJournal(state) {
            state.journal.splice(0)
        }
    }
    actions: ActionTree<IActivityStoreData, IGlobalStateData> & IActivityStoreActions = {
        notify: ({ commit }, { source, text, hint, type, toBeClosed = true }) => {
            commit(this.mutations.notify.name, new NotificationContext(source, text, type, hint, toBeClosed))
        },
        add_log: ({commit}, message) => {
            commit(this.mutations.addLog.name, message)
        },
        clear_log: ({commit}) => {
            commit(this.mutations.clearLog.name)
        },
        clear_journal: ({ commit }) => {
            commit(this.mutations.clearJournal.name)
        }
    }
    getters: GetterTree<IActivityStoreData, IGlobalStateData> & IActivityStoreGetters = {
        Notifications: state => state.notifications,
        Log: state => state.log,
        Journal: state => state.journal
    }

    async cleanup() {}

    preprocess_state(data: any) {
        return Object.assign(this.defaultState, data)
    }
}

export default ActivityStore