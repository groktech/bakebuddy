import IGlobalStateData from "@render/store/sub-stores/IGlobalStateData";
import { ActionContext } from "vuex";

export type StateData = IGlobalStateData & IActivityStoreData;
export type Context = ActionContext<IActivityStoreData, IGlobalStateData>;

export enum ENotificationType {
  success = "SUCCESS",
  failure = "FAILURE",
  neutral = "INFO",
  warning = "WARNING",
}

export interface INotificationContext {
  id: string;
  source: string;
  text: string;
  hint: string;
  type: ENotificationType;
  close: () => void;
}

export interface IJournalEvent {
  dateTime: string;
  type: ENotificationType;
  source: string;
  text: string;
}

export interface IActivityStoreData {
  notifications: Array<INotificationContext>;
  journal: Array<IJournalEvent>;
  log: Array<string>;
}

export interface AmiLogMsg { msg: string, module?: string, level: string, timestamp: number }

export interface IActivityStoreMutations {
  notify: (
    state: IActivityStoreData,
    notification: INotificationContext
  ) => void;
  removeNotification: (state: IActivityStoreData, id: string) => void;
  addLog: (state: IActivityStoreData, message: string) => void;
  clearLog: (state: IActivityStoreData) => void;
  clearJournal: (state: IActivityStoreData) => void;
}

export interface IActivityStoreActions {
  notify: (
    { commit }: Context,
    {
      text,
      type,
      hint,
      toBeClosed,
      source,
    }: {
      source: string;
      text: string;
      type: ENotificationType;
      hint?: string;
      toBeClosed?: boolean;
    }
  ) => void;
  add_log: ({ commit }: Context, message: AmiLogMsg) => void;
  clear_log: ({ commit }: Context) => void;
  clear_journal: ({ commit }: Context) => void;
}

export interface IActivityStoreGetters {
  Notifications: (state: IActivityStoreData) => INotificationContext[];
  Log: (state: IActivityStoreData) => Array<string>
  Journal: (state: IActivityStoreData) => Array<IJournalEvent>;
}
