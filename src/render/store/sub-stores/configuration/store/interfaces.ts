import IGlobalStateData from '@render/store/sub-stores/IGlobalStateData'
import { ActionContext } from 'vuex';

export type StateData = IGlobalStateData | IConfigurationStoreData
export type Context = ActionContext<StateData, StateData>
export type ConfiguredColor = { id: string, color: string }
export type Theme = { [key: string]: string }

export enum EConfigurationOptions {
    NotificationTimeout = "NotificationTimeout",
    NotificationCountLimit = "NotificationCountLimit",
    JournalCapacity = "JournalCapacity",
    BootstrapUrl = "BootstrapUrl",
    BootstrapBlockHash = "BootstrapBlockHash",
    BlockNumber = "BlockNumber",
    DerivPath = "DerivPath",
    ConfigFile = "ConfigFile",
    BakeRightsFeedSource = "BakeRightsFeedSource",
    EndorsingRightsFeedSource = "EndorsingRightsFeedSource",
    MainChainId = "MainChainId"
}

export interface IConfigurationStoreData {
    Themes: { [key: string]: Theme },
    Colors: { [key: string]: string },
	BackgroundImage: string | undefined,
	FontSize: string | undefined,
    SelectedTheme: string,
    Options: { [key in EConfigurationOptions]: any }
}

export interface IConfigurationStoreMutations {
    setColor: (state: IConfigurationStoreData, color: ConfiguredColor) => void
    setBackgroundImage: (state: IConfigurationStoreData, image: string | undefined) => void,
	setFontSize: (state:  IConfigurationStoreData, size: string | undefined) => void
    removeColor: (state: IConfigurationStoreData, id: string) => void
    createTheme: (state: IConfigurationStoreData, { id, data }: { id: string, data?: Theme }) => void
    switchTheme: (state: IConfigurationStoreData, id: string) => void
    removeTheme: (state: IConfigurationStoreData, id: string) => void
    importTheme: (state: IConfigurationStoreData, { id, theme }: { id: string, theme: Theme }) => void
    //setConfigurationOptions: (state: IConfigurationStoreData, options: ConfigurationOptions) => void
    setConfigurationOption: (state: IConfigurationStoreData, { name, value }: { name: EConfigurationOptions, value: any }) => void
}

export interface IConfigurationStoreActions {
    setColor: ({ commit }: Context, color: ConfiguredColor) => void,
    setBackgroundImage: ({ commit }: Context, image: string | undefined) => void,
	setFontSize: ({ commit }: Context, size: string | undefined) => void
    removeColor: ({ commit }: Context, id: string) => void,
    createTheme: ({ commit }: Context, { id, data }: { id: string, data?: { [key: string]: string } }) => void,
    switchTheme: ({ commit }: Context, id: string) => void,
    removeTheme: ({ commit }: Context, id: string) => void,
    importTheme: ({ commit }: Context, { id, theme }: { id: string, theme: Theme }) => void,
    //setConfigurationOptions: ({ commit }: Context, options: ConfigurationOptions) => void
    setConfigurationOption: ({ commit }: Context, { name, value }: { name: EConfigurationOptions, value: any }) => void
}

export interface IConfigurationStoreGetters {
    Colors: (state: IConfigurationStoreData) => ConfiguredColor[],
    BackgroundImage: (state: IConfigurationStoreData) => string | undefined,
    ColorsIds: (state: IConfigurationStoreData) => string[],
    Color: (state: IConfigurationStoreData) => (id: string) => string
    ThemesIds: (state: IConfigurationStoreData) => string[],
    Theme: (state: IConfigurationStoreData) => (id: string) => Theme,
    SelectedTheme: (state: IConfigurationStoreData) => string
    Option: (state: IConfigurationStoreData) => (id: EConfigurationOptions) => any
	FontSize: (state: IConfigurationStoreData) => string | undefined
}
