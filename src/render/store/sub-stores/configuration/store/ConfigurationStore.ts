import { GetterTree, ActionTree, MutationTree } from 'vuex'
import { IConfigurationStoreData, IConfigurationStoreMutations, IConfigurationStoreActions, IConfigurationStoreGetters, EConfigurationOptions } from './interfaces'
import { cloneDeep } from 'lodash'
import IGlobalStateData from '../../IGlobalStateData'
import PersistentStore from '@render/store/db/PersistentStore'

class ConfigurationStore extends PersistentStore<IConfigurationStoreData, IGlobalStateData, IConfigurationStoreMutations, IConfigurationStoreActions, IConfigurationStoreGetters> {
    state: IConfigurationStoreData

    constructor(sourceFile: string) {
        super(sourceFile)
        this.state = this.defaultState
    }

    serialize(data: IConfigurationStoreData): Uint8Array | string {
        return JSON.stringify(data)
    }
    deserialize(data: Uint8Array): Promise<IConfigurationStoreData> {
        throw new Error('Method not implemented.')
    }


    get defaultState() { return ConfigurationStore.defaultState }
    static get defaultState() {
        const result: IConfigurationStoreData & { [key: string]: any } = cloneDeep({
            Colors: {},
            BackgroundImage: undefined,
            FontSize: undefined,
            Themes: { Dark: {} },
            SelectedTheme: "",
            Plugins: [],
            Options: {
                [EConfigurationOptions.NotificationTimeout]: 10000,
                [EConfigurationOptions.NotificationCountLimit]: 10,
                [EConfigurationOptions.JournalCapacity]: 999,
                [EConfigurationOptions.BootstrapUrl]: "",
                [EConfigurationOptions.BootstrapBlockHash]: "",
                [EConfigurationOptions.BlockNumber]: "",
                [EConfigurationOptions.DerivPath]: "",
                [EConfigurationOptions.ConfigFile]: "",
                [EConfigurationOptions.BakeRightsFeedSource]: "",
                [EConfigurationOptions.EndorsingRightsFeedSource]: "",
                [EConfigurationOptions.MainChainId]: ""
            }
        })
        return result
    }
    mutations: MutationTree<IConfigurationStoreData> & IConfigurationStoreMutations = {
        setColor: function (state, { id, color }) {
            if (state.Colors === undefined) {
                state.Colors = {}
            }
            state.Colors[id] = color
        },
        setBackgroundImage: function (state, image) {
            state.BackgroundImage = image
        },
        setFontSize: function (state, size) {
            state.FontSize = size
        },
        removeColor: function (state, id) {
            delete state.Colors[id]
        },
        createTheme: function (state, { id, data }) {
            // TODO:
            //@ts-ignore
            state.Themes[id] = data ?? { ...state.Colors, BackgroundImage: state.BackgroundImage, FontSize: state.FontSize }
            state['SelectedTheme'] = id
        },
        importTheme: function (state, { id, theme }) {
            state.Themes[id] = theme
        },
        switchTheme: function (state, id) {
            const themeColors = state.Themes[id] ?? {}
            for (const colorId of Object.keys(themeColors)) {
                state.Colors[colorId] = themeColors[colorId]
            }
            for (const colorId of Object.keys(state.Colors)) {
                if (themeColors[colorId] === undefined)
                    delete state.Colors[colorId]
            }
            state.BackgroundImage = state.Themes[id].BackgroundImage
            state.FontSize = state.Themes[id].FontSize
            state.SelectedTheme = id
        },
        removeTheme: function (state, id) {
            delete state.Themes[id]
            state.SelectedTheme = ""
        },
        setConfigurationOption: (state, { name, value }) => {
            state.Options[name] = value
        }
    }
    actions: ActionTree<IConfigurationStoreData, IGlobalStateData> & IConfigurationStoreActions = {
        setColor: ({ commit }, color) => {
            commit(this.mutations.setColor.name, color)
            document.getElementById("app")?.style.setProperty(color.id, color.color)
        },
        setBackgroundImage: ({ commit }, image) => {

            commit(this.mutations.setBackgroundImage.name, image)
            if (image === undefined) {
                document.getElementById("bg")?.style.removeProperty("--background-image")
            } else {
                document.getElementById("bg")?.style.setProperty("--background-image", image)
            }
        },
        setFontSize: ({ commit }, size) => {
            commit(this.mutations.setFontSize.name, size)
            if (size === undefined || size === '') {
                const tags = document.getElementsByTagName("html")
                if (tags.length > 0) {
                    //@ts-ignore
                    tags[0].style.fontSize = null;
                }
            } else {
                const tags = document.getElementsByTagName("html")
                if (tags.length > 0) {
                    tags[0].style.fontSize = size;
                }
            }
        },
        removeColor: ({ commit }, id) => {
            commit(this.mutations.removeColor.name, id)
            document.getElementById("app")?.style.removeProperty(id)
        },
        createTheme: ({ commit, dispatch }, { id, data }) => {
            commit(this.mutations.createTheme.name, { id, data })
            dispatch("switchTheme", id)
        },
        importTheme: ({ commit }, { id, theme }) => {
            commit(this.mutations.importTheme.name, { id, theme })
        },
        switchTheme: (context, id) => {
            const { commit, state } = context
            const oldColors = cloneDeep(state.Colors)
            commit(this.mutations.switchTheme.name, id)
            const root = document.getElementById("app")
            const colors = state.Colors
            for (const colorId of Object.keys(colors ?? {})) {
                root?.style.setProperty(colorId, colors[colorId]);
            }
            for (const colorId of Object.keys(oldColors ?? {})) {
                if (colors[colorId] === undefined)
                    root?.style.removeProperty(colorId);
            }
            this.actions.setBackgroundImage(context, state.BackgroundImage);
            this.actions.setFontSize(context, state.FontSize)
        },
        removeTheme: ({ commit }, id) => {
            commit(this.mutations.removeTheme.name, id)
        },
        setConfigurationOption: ({ commit }, { name, value }) => {
            commit(this.mutations.setConfigurationOption.name, { name, value })
        }
    }
    getters: GetterTree<IConfigurationStoreData, IGlobalStateData> & IConfigurationStoreGetters = {
        Colors: state => Object.keys(state.Colors ?? {}).map(id => { return { id, color: state.Colors[id] } }),
        ColorsIds: state => Object.keys(state.Colors ?? {}),
        Color: state => id => (state.Colors ?? {})[id],
        ThemesIds: state => Object.keys(state.Themes ?? {}),
        Theme: state => id => state.Themes[id],
        SelectedTheme: state => state.SelectedTheme,
        Option: state => id => state.Options[id],
        BackgroundImage: state => state.BackgroundImage,
        FontSize: state => state.FontSize
    }

    preprocess_state(data: any) {
        return data
    }

    async loadState(strict?: boolean) {
        await super.loadState(strict)

        const defaultState = this.defaultState
        const state = this.state as { [key: string]: any }
        for (const configOption of Object.keys(defaultState)) {
            if ((state[configOption] as any) === undefined) {
                state[configOption] = defaultState[configOption]
            }
        }
        const root = document.getElementById("app")
        const colors = this.state.Colors
        for (const colorId of Object.keys(colors ?? {})) {
            root?.style.setProperty(colorId, colors[colorId]);
        }

        const bg = document.getElementById("bg")
        if (this.state.BackgroundImage !== undefined)
            bg?.style.setProperty("--background-image", this.state.BackgroundImage)


        if (this.state.Options === undefined) this.state.Options = defaultState.Options
        else {
            for (const option of Object.keys(defaultState.Options) as Array<EConfigurationOptions>) {
                if (this.state.Options[option] === undefined) {
                    this.state.Options[option] = defaultState.Options[option] as never // it respects data type, just TS does not understand ... whatever
                }
            }
        }
    }
}

export default ConfigurationStore