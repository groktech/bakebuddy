import { IAppStoreData } from './account/store/interfaces';
import { IConfigurationStoreData } from './configuration/store/interfaces';

type IGlobalStateData = IAppStoreData & IConfigurationStoreData
export default IGlobalStateData