import { Module, ActionContext } from 'vuex';
import IGlobalStateData from '@render/store/sub-stores/IGlobalStateData';
import { IConfigurationStoreData } from '@render/store/sub-stores/configuration/store/interfaces';
import IPersistantStore from '@render/store/db/IPersistentStore';

export type Context = ActionContext<IAppStoreData, IAppStoreData>
export type IAppModuleState = IAppStoreData | IConfigurationStoreData

export interface IAppStoreData { }

export interface IAccountStoreMutations {
    registerPersistantModule: (state: IAppStoreData, { id, m }: { id: string, m: Module<IAppModuleState, IGlobalStateData> & IPersistantStore }) => void,
    unregisterPersistantModule: (state: IAppStoreData, id: string) => void
    registerVolatileModule: (state: IAppStoreData, { id, m }: { id: string, m: Module<IAppModuleState, IGlobalStateData> & IPersistantStore }) => void,
    unregisterVolatileModule: (state: IAppStoreData, id: string) => void
}

export interface IAccountStoreActions {
    load: ({ commit, state, dispatch }: Context) => void,
    cleanup: ({ commit }: Context) => void
}

export interface IAccountStoreGetters {
}
