import { Store, Commit } from 'vuex'
import { IAppStoreData, IAccountStoreMutations, IAccountStoreActions as IAppStoreActions, IAccountStoreGetters as IAppStoreGetters } from './interfaces'
import PersistantStoresPlugin from '../../../db/PersistantStoresPlugin'
import ConfigurationStore from '../../configuration/store/ConfigurationStore'
import { STORE_CONFIGURATION_FILE, STORE_JOURNAL_FILE } from '@render/core/common/constants'
import IGlobalStateData from '../../IGlobalStateData'
import { trace, error } from '@render/common/log'
import ActivityStore from '../../actions/store/ActivityStore'

const persistantStoresPlugin = new PersistantStoresPlugin()

class AppStore {
	configurationStore?: ConfigurationStore
	activityStore?: ActivityStore

	get defaultState() {
		return {} as IAppStoreData
	}

	mutations: IAccountStoreMutations = {
		registerPersistantModule: function (this: Store<IGlobalStateData>, state, { id, m }) {
			persistantStoresPlugin.registerModule(id, m)
			this.registerModule(id, m)
		},
		unregisterPersistantModule: function (this: Store<IGlobalStateData>, state, id) {
			if (persistantStoresPlugin.hasModule(id)) {
				persistantStoresPlugin.unregisterModule(id)
				this.unregisterModule(id)
			}
		},
		registerVolatileModule: function (this: Store<IGlobalStateData>, state, { id, m }) {
			this.registerModule(id, m)
		},
		unregisterVolatileModule: function (this: Store<IGlobalStateData>, state, id) {
			if (this.hasModule(id))
				this.unregisterModule(id)
		},
	}
	actions: IAppStoreActions = {
		load: async ({ commit, state, dispatch }) => {
			try {
				this.configurationStore = new ConfigurationStore(STORE_CONFIGURATION_FILE)
				this.activityStore = new ActivityStore(STORE_JOURNAL_FILE)

				try {
					await Promise.all([
						this.configurationStore.loadState(),
						this.activityStore.loadState(false)
					])
				} catch (err){
					console.log("saving", err)
					this.configurationStore.saveState(),
					this.activityStore.saveState()
				}

				commit(this.mutations.registerPersistantModule.name, { id: 'configuration', m: this.configurationStore })
				commit(this.mutations.registerPersistantModule.name, { id: 'activities', m: this.activityStore })

				if (await window.os.get_env("NODE_ENV") === 'development')
					trace(JSON.stringify(state))
			} catch (err) {
				dispatch(this.actions.cleanup.name)
			}

		},
		cleanup: async ({ commit }) => {
			commit(this.mutations.unregisterPersistantModule.name, 'configuration')
			commit(this.mutations.unregisterPersistantModule.name, 'activities')
			this.activityStore?.cleanup()
			this.configurationStore = undefined
		}
	}
	getters: IAppStoreGetters = {
	}
	plugins: ((store: Store<any>) => void)[] = [
		persistantStoresPlugin.plugin
	]
}

export default AppStore