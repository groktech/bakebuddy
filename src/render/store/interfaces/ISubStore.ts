import { Module, MutationTree, ActionTree, GetterTree } from 'vuex'

interface ISubStore<S, R, M, A, G> extends Module<S, R> {
    state: S | (() => S)
    mutations: MutationTree<S> & M
    actions: ActionTree<S, R> & A
    getters: GetterTree<S, R> & G
    saveState: () => void
    //json2State: (data: string) => IGlobalStateData | IMachineStoreData | IConfigurationStoreData
}

export default ISubStore