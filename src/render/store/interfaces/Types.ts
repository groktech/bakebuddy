import { ActionHandler, ActionContext } from 'vuex'
import { IAccountStoreMutations, IAccountStoreActions, IAccountStoreGetters } from '../sub-stores/account/store/interfaces'
import { IConfigurationStoreMutations, IConfigurationStoreActions, IConfigurationStoreGetters } from '../sub-stores/configuration/store/interfaces'
import { IActivityStoreGetters, IActivityStoreActions, IActivityStoreMutations } from '../sub-stores/actions/store/interfaces'

type PromiseOf<T> = T extends Promise<any> ? T : Promise<T>
type OrEmpty<T> = T extends {} ? T : {}

export interface IStoreActionsList {
    [name: string]: ActionHandler<any, any>
}

export interface IStoreGetterList<S = any, G = any> {
    [name: string]: (state: S, getters: G, rootState: any, rootGetters: any) => any
}

export interface IStoreMutationList<S = any, G = any> {
    [name: string]: (state: S, payload: any) => void
}

interface IStoreActions<S = any> {
    actions?: IStoreActionsList
}

interface IStoreGetters<S = any> {
    getters?: IStoreGetterList
}

interface IStoreMutations<S = any> {
    mutations?: IStoreMutationList
}

type ToDirectActions<T extends IStoreActionsList> = {
    [K in keyof T]: Parameters<T[K]>[1] extends undefined
    ? (() => PromiseOf<ReturnType<T[K]>>)
    : (Extract<Parameters<T[K]>[1], undefined> extends never ?
        ((payload: Parameters<T[K]>[1]) => PromiseOf<ReturnType<T[K]>>) :
        ((payload?: Parameters<T[K]>[1]) => PromiseOf<ReturnType<T[K]>>))
}

export type DirectActions<O extends IStoreActions> = ToDirectActions<OrEmpty<O["actions"]>>

type ToDirectGetters<T extends IStoreGetterList> = {
    [K in keyof T]: ReturnType<T[K]>
}

export type DirectGetters<O extends IStoreGetters> = ToDirectGetters<OrEmpty<O["getters"]>>

export type ToDirectMutations<T extends IStoreMutationList> = {
    [K in keyof T]: Parameters<T[K]>[1] extends undefined
    ? (() => void)
    : (Extract<Parameters<T[K]>[1], undefined> extends never ?
        ((payload: Parameters<T[K]>[1]) => void) :
        ((payload?: Parameters<T[K]>[1]) => void))
}

export type DirectMutations<O extends IStoreMutations> = ToDirectMutations<OrEmpty<O["mutations"]>>

export type AllGetters = IAccountStoreGetters &
    IConfigurationStoreGetters &
    IActivityStoreGetters
export type AllMutations = IAccountStoreMutations &
    IConfigurationStoreMutations &
    IActivityStoreMutations
export type AllActions = IAccountStoreActions &
    IConfigurationStoreActions &
    IActivityStoreActions

