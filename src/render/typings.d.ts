// declare const React: string
declare module '*.json'
declare module '*.png'
declare module '*.jpg'

interface IWindowOs {
	get_env: (name: string) => Promise<string | undefined>
	tmpdir: () => Promise<string>
	homedir: () => Promise<string>
	cwd: () => Promise<string>
	open: (url: string) => Promise<void>
}

interface IWindowNet {
	download_file: (sourceUrl: string, targetPath: string) => Promise<void>
	download_data: (sourceUrl: string) => Promise<any>
	is_ip: (ip: string) => Promise<number>
}

interface IWindowApp {
	loaded: () => void
}

interface IWindowFs {
	readFile: (path: string, options?: {
		encoding?: string;
		flag?: string;
	}) => Promise<Uint8Array>
	readdir: (path: string) => Promise<Array<string>>
	mkdirp: (path: string) => Promise<void>
	realpath: (path: string, options?: { encoding: BufferEncoding }) => Promise<string>
	writeFile: (path: string, data: any, options?: any) => Promise<void>
	rename: (oldPath: string, newPath: string) => Promise<void>
	exists: (path: string) => Promise<boolean>
	statIsFile: (path: string) => Promise<boolean>
	statIsDirectory: (path: string) => Promise<boolean>
	sha256sum_file: (path: string) => Promise<string>
	sha256sum_data: (data: Uint8Array) => Promise<string>
}

interface IWindowClipboard {
	write: (data: string) => Promise<void>
}

interface IWindowLog {
	trace: (msg: any) => Promise<void>,
	debug: (msg: any) => Promise<void>,
	info: (msg: any) => Promise<void>,
	warn: (msg: any) => Promise<void>,
	error: (msg: any) => Promise<void>,
	fatal: (msg: any) => Promise<void>,
}

interface IWindowProc {
	exec: (cmd: string, options?: any) => Promise<{ stdout: string, stderr: string }>
	spawn: ({ bin, args }: { bin: string, args: Array<string> }, _options: any, _await: boolean) => Promise<string | number>
	sudo: (cmd: string) => { stdout: string | ArrayBuffer | Uint8Array, stderr: string | ArrayBuffer | Uint8Array }
}

interface Window {
	os: IWindowOs,
	net: IWindowNet,
	app: IWindowApp,
	fs: IWindowFs,
	clipboard: IWindowClipboard,
	log: IWindowLog,
	proc: IWindowProc
}
