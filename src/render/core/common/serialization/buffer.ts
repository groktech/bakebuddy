import { Binary, serialize } from 'bson';

const getShift = (index: number) => {
    let shift = 234
    switch (index % 5) {
        case 1: shift = 56; break;
        case 2: shift = 128; break;
        case 3: shift = 96; break;
        case 4: shift = 176; break;
    }
    return shift
}

export const numberToBytes = (n: number, index: number): Uint8Array => {
    const byteArray: Array<number> = [];
    let length = 0
    while (n > 0) {
        const byte = n & 0xff;
        byteArray.splice(0, 0, byte);
        n = (n - byte) / 256
        length++
    }

    return Uint8Array.from([getShift(index) - length, ...byteArray])
}

export const bytesToNumber = (data: Uint8Array, index: number): { n: number, remainer: Uint8Array } => {
    const length = getShift(index) - data[0]
    const remainer = data.slice(1 + length)
    const numberBytes = data.slice(1, length + 1)

    let n = 0;
    for (let i = 0; i < length; i++) {
        n = (n * 256) + numberBytes[i];
    }
    return { n, remainer }
}

export const serializeUint8Array = (data: Uint8Array, index: number): Uint8Array => {
    return concat_Uint8Arrays([numberToBytes(data.length, index), data])
}

export const serializeUint8Arrays = (data: Uint8Array | Array<Uint8Array>): Uint8Array => {
    if (!Array.isArray(data)) {
        data = [data]
    }
    let index = 1;
    return data.reduce((res, b) => concat_Uint8Arrays([res, serializeUint8Array(b, index++)]), numberToBytes(data.length, 0))
}

export const deserializeUint8Array = (data: Uint8Array, index: number): { data: Uint8Array, remainer: Uint8Array } => {
    const { n: length, remainer } = bytesToNumber(data, index)
    return { data: remainer.slice(0, length), remainer: remainer.slice(length) }
}

export const deserializeUint8Arrays = (data: Uint8Array, multi?: boolean): Uint8Array | Array<Uint8Array> => {
    const results: Array<Uint8Array> = []
    const { n: length, remainer } = bytesToNumber(data, 0)
    let remains = remainer
    for (let i = 0; i < length; i++) {
        const { data, remainer: _remainer } = deserializeUint8Array(remains, i + 1)
        results.push(data)
        remains = _remainer
    }
    return length === 1 && !multi ? results[0] : results
}

/**
 * Converts data into Uint8Array if they are instace of Binary. Otherwise returns data without changes
 * @param data 
 * @returns 
 */
export const binary_2_Uint8Array = (data: any) => {
    if (data instanceof Binary) {
        return Uint8Array.from((data as Binary).buffer)
    }
    return data
}

export const convert_binary_2_Uint8Array = (obj: any | Array<any>, deep: boolean = true) => {
    if (obj instanceof Binary) {
        return binary_2_Uint8Array(obj)
    }

    if (Array.isArray(obj)) {
        for (let i = 0; i < obj.length; i++) {
            obj[i] = binary_2_Uint8Array(obj[i])
        }
        return obj
    }

    for (const key of Object.keys(obj)) {
        if (obj[key] instanceof Binary) {
            obj[key] = binary_2_Uint8Array(obj[key])
        } else if (deep && (typeof obj[key] === 'object' || Array.isArray(obj[key]))) {
            obj[key] = convert_binary_2_Uint8Array(obj[key], deep)
        }
    }
    return obj;
}

export interface ICredentialsStoredData {
    type?: EProtectedDataType,
    data: any
}

export enum EProtectedDataType {
    string,
    buffer,
    file,
    json
}

export const concat_Uint8Arrays = (parts: Uint8Array[]) => {
    let position = 0
    const length = parts.reduce((last, curr) => last + curr.length, 0)
    const result = new Uint8Array(length)
    for (const part of parts) {
        result.set(part, position)
        position += part.length
    }
    return result
}

export function decode_from_Uint8Array(value: Uint8Array | string): string
export function decode_from_Uint8Array(value: Uint8Array, type: "string"): string
export function decode_from_Uint8Array(value: Uint8Array, type: "boolean"): boolean
export function decode_from_Uint8Array(value: Uint8Array | string, type: "string" | "boolean" = "string") {
    switch (type) {
        case "boolean":
            return value[0] == 1
        default:
            if (typeof value === "string")
                return value;
            const decoder = new TextDecoder()
            return decoder.decode(value)
    }
}

export const encode_to_Uint8Array = (value: string | boolean) => {
    switch (typeof value) {
        case "boolean":
            return Uint8Array.from([value ? 1 : 0])
        default:
            const encoder = new TextEncoder()
            return encoder.encode(value)
    }
}

export const Uint8Array2Hex = (value: Uint8Array) => {
    return [...value].map(x => x.toString(16).padStart(2, '0')).join('');
}

export const build_decoded_Uint8Array = (value: Uint8Array[]) => {
    return decode_from_Uint8Array(concat_Uint8Arrays(value))
}