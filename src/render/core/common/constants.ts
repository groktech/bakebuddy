import _package from "@root/package.json"

export const STORE_CONFIGURATION_FILE = "configuration.json"
export const STORE_JOURNAL_FILE = "journal.txt"
export const BB_VERSION = _package.version;