import { Uint8Array2Hex } from "./serialization/buffer";

type TypedArray = Int8Array | Int16Array | Int32Array | Uint8Array | Uint16Array | Uint32Array | Uint8ClampedArray | Float32Array | Float64Array | DataView | ArrayBuffer

export async function sha256sum(data: TypedArray): Promise<Uint8Array>
export async function sha256sum(data: TypedArray, format: 'bytes'): Promise<Uint8Array>
export async function sha256sum(data: TypedArray, format: 'buffer'): Promise<ArrayBuffer>
export async function sha256sum(data: TypedArray, format: 'hex'): Promise<string>
export async function sha256sum(data: TypedArray, format: 'hex' | 'bytes' | 'buffer' = 'bytes'): Promise<Uint8Array | ArrayBuffer | string> {
    const result = await crypto.subtle.digest('SHA-256', data);
    switch(format) {
        case "hex":
            const bytes = new Uint8Array(result)
            return Uint8Array2Hex(bytes)
        case "bytes":
            return new Uint8Array(result)
        case "buffer":
            return result;
    }
}


