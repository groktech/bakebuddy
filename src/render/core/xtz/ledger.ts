import store from "@render/store/store";
import {
  AmiLogMsg,
  ENotificationType,
} from "@render/store/sub-stores/actions/store/interfaces";
import { exec_sudo, spawn } from "../ipc/proc";
import { first } from "lodash";
import { collect_app_info, signerInfo } from "./status";
import { error, info } from "@render/common/log";
import { exists, readFile, writeFile } from "../ipc/fs";
import { v4 as uuidv4 } from "uuid";

type LedgerConfigureOptions = {
  derivPath?: string;
  blockNumber?: number | string;
  mainChainId?: string;
  bakerAddr?: string;
  log: (msg: AmiLogMsg) => void;
  eventHook?: (event: string) => void;
};

const _decoder = new TextDecoder();

const _process_ami_output = (options: LedgerConfigureOptions) => {
  return (output: Uint8Array | ArrayBuffer) => {
    const log = _decoder.decode(output);
    const lines = log.split("\n");
    for (const line of lines) {
      if (!line) continue;
      try {
        var logObj = JSON.parse(line.trim());
        options.log(logObj);
      } catch (err) {}
    }
  };
};

const _read_log_file = async (
  file: string,
  lastContent: string,
  options: LedgerConfigureOptions
) => {
  if (!(await exists(file))) return "";
  const log = _decoder.decode(await readFile(file));
  try {
    const newLog = log.replace(lastContent, "");
    const lines = newLog.split("\n");
    for (const line of lines) {
      if (!line) continue;
      const status = line.match(/### STATUS: (.*) ###/);
      if (status) {
        if (typeof options.eventHook === "function") {
          options.eventHook(status[1]);
        }
      } else {
        try {
          var logObj = JSON.parse(line.trim());
          options.log(logObj);
        } catch (err) {}
      }
    }
  } finally {
    return log;
  }
};

export const import_key = async (options: LedgerConfigureOptions) => {
  const _logFile = `/tmp/bb-import-key-${uuidv4()}.log`;
  let lastLogContent = "";
  let interval: NodeJS.Timeout | undefined = undefined;

  try {
    options.log({ msg: "Collecting signer runtime info...", module: "bb", level: "info", timestamp: Math.floor(Date.now() / 1000) | 0 })
    const _signerInfo = await collect_app_info("signer");
    const _isSignerRunning = _signerInfo.signer == "running";

    let _importKeyScript = ``;
    if (_isSignerRunning)
      _importKeyScript += `ami --path=/bake-buddy/signer stop && `;
    _importKeyScript += `ami --path=/bake-buddy/signer import-key --force `;
    if (options.derivPath) {
      _importKeyScript += `--derivation-path=${options.derivPath} `;
    }
    _importKeyScript += " && ";
    _importKeyScript += `ami --path=/bake-buddy/signer start && `;
    _importKeyScript += `ami --path=/bake-buddy/node import-key tcp://127.0.0.1:2222/$(grep  'value": ' /bake-buddy/signer/data/.tezos-signer/public_key_hashs | sed 's/.*value": "\\(.*\\)".*$/\\1/g') --force `;

    await writeFile("/tmp/bb-import-key.sh", _importKeyScript);
    options.log({ msg: "Confirm key import 2x (signer & client)", module: "bb", level: "info", timestamp: Math.floor(Date.now() / 1000) | 0 })


    if (await exists(_logFile))
      lastLogContent = _decoder.decode(await readFile(_logFile));
    interval = setInterval(async () => {
      lastLogContent = await _read_log_file(_logFile, lastLogContent, options);
    }, 1000);
    await exec_sudo(`sh /tmp/bb-import-key.sh > ${_logFile} 2>&1`);
    await _read_log_file(_logFile, lastLogContent, options);
    options.log({ msg: "Import key successfully completed!", module: "bb", level: "info", timestamp: Math.floor(Date.now() / 1000) | 0 })

    info("Import key completed");
    store.Actions.notify({
      text: "Import key completed!",
      type: ENotificationType.success,
      source: "BB",
    });
    return true;
  } catch (err: any) {
    error("Stop failed: " + err.message);
    store.Actions.notify({
      text: "Import key failed!",
      hint: err.message,
      source: "BB",
      type: ENotificationType.failure,
      toBeClosed: false,
    });
    await _read_log_file(_logFile, lastLogContent, options);
  } finally {
    if (interval) clearInterval(interval);
  }
  return false;
};

export const register_key = async (options: LedgerConfigureOptions) => {
  const _args2 = [
    "--path=/bake-buddy/signer",
    "client",
    "register",
    "key",
    "baker",
    "as",
    "delegate",
  ];

  let stderrOutput = "";
  const _proces_stderr = _process_ami_output(options);
  const _stderr = (output: Uint8Array | ArrayBuffer) => {
    _proces_stderr(output);
    stderrOutput = stderrOutput + _decoder.decode(output);
  };

  const _result2 = await spawn(
    "ami",
    _args2,
    {
      stdout: _process_ami_output(options),
      stderr: _stderr,
    },
    true
  );
  if (_result2 !== 0 || stderrOutput.match("Error:")) {
    if (stderrOutput.match("Error:")) {
      store.Actions.add_log({
        module: "BB",
        msg:
          "Failed to register key: " +
          (first(stderrOutput.match(/Error:([^]*)/gm)) || "unknown"),
        level: "error",
        timestamp: Math.floor(Date.now() / 1000) | 0,
      });
    } else {
      store.Actions.add_log({
        module: "bb",
        msg: "Failed to register key!",
        level: "error",
        timestamp: Math.floor(Date.now() / 1000) | 0,
      });
    }
    store.Actions.notify({
      text: "Failed to register the key!",
      source: "BB",
      type: ENotificationType.failure,
      toBeClosed: false,
    });
    return;
  }
  store.Actions.add_log({
    module: "BB",
    msg: "Key registration successful!",
    level: "success",
    timestamp: Math.floor(Date.now() / 1000) | 0,
  });
  store.Actions.notify({
    text: "Key registration successful!",
    type: ENotificationType.success,
    source: "BB",
  });
};

export const setup_ledger = async (options: LedgerConfigureOptions) => {
  const _args2 = [
    "--path=/bake-buddy/signer",
    "client",
    "setup",
    "ledger",
    "to",
    "bake",
    "for",
    "baker",
  ];
  if (!isNaN(Number(options.blockNumber))) {
    _args2.push(`--main-hwm`);
    _args2.push(options.blockNumber?.toString() ?? "");
  }
  if (options.mainChainId) {
    _args2.push(`--main-chain-id`);
    _args2.push(options.mainChainId);
  }

  let stderrOutput = "";
  const _proces_stderr = _process_ami_output(options);
  const _stderr = (output: Uint8Array | ArrayBuffer) => {
    _proces_stderr(output);
    stderrOutput = stderrOutput + _decoder.decode(output);
  };

  const _result2 = await spawn(
    "ami",
    _args2,
    {
      stdout: _process_ami_output(options),
      stderr: _stderr,
    },
    true
  );
  if (_result2 !== 0 || stderrOutput.match("Error:")) {
    if (stderrOutput.match("Error:")) {
      store.Actions.add_log({
        module: "BB",
        msg:
          "Failed to setup ledger: " +
          (first(stderrOutput.match(/Error:([^]*)/gm)) || "unknown"),
        level: "error",
        timestamp: Math.floor(Date.now() / 1000) | 0,
      });
    } else {
      store.Actions.add_log({
        module: "bb",
        msg: "Failed to setup ledger!",
        level: "error",
        timestamp: Math.floor(Date.now() / 1000) | 0,
      });
    }
    store.Actions.notify({
      text: "Failed to setup ledger!",
      source: "BB",
      type: ENotificationType.failure,
      toBeClosed: false,
    });
    return;
  }
  store.Actions.add_log({
    module: "BB",
    msg: "Ledger setup successful!",
    level: "success",
    timestamp: Math.floor(Date.now() / 1000) | 0,
  });
  store.Actions.notify({
    text: "Ledger setup successful!",
    type: ENotificationType.success,
    source: "BB",
  });
};
