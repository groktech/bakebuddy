import { error, info } from "@render/common/log";
import store from "@render/store/store";
import { get_current_user } from "../actions";
import { exists, readFile, writeFile } from "../ipc/fs";
import { exec_sudo } from "../ipc/proc";
import { v4 as uuidv4 } from "uuid";
import {
  AmiLogMsg,
  ENotificationType,
} from "@render/store/sub-stores/actions/store/interfaces";
import { Report } from "./interfaces/Report";
import { basename } from "../common/path";

export const generate_baker_config = (configFile: string, user: string) => {
  if (configFile.length > 2) {
    configFile = `CONFIG_FILE: ${configFile}`;
  }
  return `
{
	id: bb_baker
	type: xtz.node
	configuration: {
		NODE_TYPE: baker
	    ${configFile}
	}
	user: ${user}
}
`;
};

export const generate_signer_config = (
  blockNumber: string | number,
  user: string
) => {
  if (!blockNumber) blockNumber = "auto";
  return `
{
	id: bb_signer
	type: xtz.signer
	configuration: {
		BLOCK_NUMBER: ${blockNumber}
	}
	user: ${user}
}
`;
};

//TODO: remove
const LOCAL_SOURCES = ""; //"--local-sources=/bake-buddy/sources.json";

type SetupOptions = {
  bootstrapUrl: string;
  bootstrapBlockHash: string;
  blockNumber: string | number;
  derivPath: string;
  isUpdate?: boolean;
  log: (msg: AmiLogMsg) => void;
  eventHook: (event: string) => void;
};

export const generate_setup_script = (
  signerConfig: string,
  bakerConfig: string,
  user: string,
  options: SetupOptions
) => {
  const wantsBootstrap =
    !options.isUpdate && options.bootstrapBlockHash && options.bootstrapUrl;
  const bootstrapSnippet = wantsBootstrap
    ? `ami --path=/bake-buddy/node ${LOCAL_SOURCES} bootstrap "${options.bootstrapUrl}" "${options.bootstrapBlockHash}" && \
	`
    : "";
  const stopSnippet = options.isUpdate
    ? `printf "### STATUS: Stop Baker ###\\n" && \
	ami --path=/bake-buddy/node ${LOCAL_SOURCES} stop && \
ami --path=/bake-buddy/signer ${LOCAL_SOURCES} stop && \
`
    : `printf "### STATUS: Stop Baker ###\\n" && \
	ami --path=/bake-buddy/node ${LOCAL_SOURCES} stop > /dev/null 2>&1;
ami --path=/bake-buddy/signer ${LOCAL_SOURCES} stop > /dev/null 2>&1;
`;
  return (
    `printf "### STATUS: Updating ami & eli ###\\n" && \
wget -q https://raw.githubusercontent.com/cryon-io/ami/master/install.sh -O /tmp/install.sh && sh /tmp/install.sh && \
` +
    stopSnippet +
    `printf "### STATUS: Creating Directories ###\\n" && \
mkdir -p /bake-buddy/node && \
mkdir -p /bake-buddy/signer && \
printf "### STATUS: Creating Configuration Files ###\\n" && \
printf '${signerConfig}' > /bake-buddy/signer/app.hjson && \
printf '${bakerConfig}' > /bake-buddy/node/app.hjson && \
printf "### STATUS: Setting up baker ###\\n" && \
ami --path=/bake-buddy/node ${LOCAL_SOURCES} setup && \
printf "### STATUS: Setting up signer ###\\n" && \
ami --path=/bake-buddy/signer ${LOCAL_SOURCES} setup && \
printf "### STATUS: Bootstrapping the baker node ###\\n" && \
` +
    bootstrapSnippet +
    `printf "### STATUS: Setting ownership ###\\n" && \
chown -R ${user}:${user} /bake-buddy/ && \
printf "### STATUS: Starting services ###\\n" && \
ami --path=/bake-buddy/node ${LOCAL_SOURCES} start && \
ami --path=/bake-buddy/signer ${LOCAL_SOURCES} start && \
printf "### STATUS: Setup completed ###\\n"
`
  );
};

const _decoder = new TextDecoder();
const _read_log_file = async (
  file: string,
  lastContent: string,
  options: SetupOptions | AmiSetupOptions
) => {
  if (!(await exists(file))) return "";
  const log = _decoder.decode(await readFile(file));
  try {
    const newLog = log.replace(lastContent, "");
    const lines = newLog.split("\n");
    for (const line of lines) {
      if (!line) continue;
      const status = line.match(/### STATUS: (.*) ###/);
      if (status) {
        if (typeof options.eventHook === "function") {
          options.eventHook(status[1]);
        }
      } else {
        try {
          var logObj = JSON.parse(line.trim());
          options.log(logObj);
        } catch (err) {}
      }
    }
  } finally {
    return log;
  }
};

export const setup_baker = async (
  configFile: string,
  options: SetupOptions
): Promise<Report> => {
  const _logFile = `/tmp/bb-setup-${uuidv4()}.log`;
  const _decoder = new TextDecoder();
  let lastLogContent = "";
  let interval: NodeJS.Timeout | undefined = undefined;

  try {
    const user = await get_current_user();
    const _signerConfig = generate_signer_config(options.blockNumber, user);
    const _bakerConfig = generate_baker_config(configFile, user);
    const _setupScript = generate_setup_script(
      _signerConfig,
      _bakerConfig,
      user,
      options
    );
    await writeFile("/tmp/bb-setup.sh", _setupScript);
    //await writeFile(_logFile, ""); // create empty file to keep user rights
    if (await exists(_logFile))
      lastLogContent = _decoder.decode(await readFile(_logFile));
    interval = setInterval(async () => {
      lastLogContent = await _read_log_file(_logFile, lastLogContent, options);
    }, 1000);
    await exec_sudo(`sh /tmp/bb-setup.sh > ${_logFile} 2>&1`);
    await _read_log_file(_logFile, lastLogContent, options);
    info("setup completed");
    store.Actions.notify({
      text: "Setup completed!",
      source: "BB",
      type: ENotificationType.success,
      toBeClosed: false,
    });
  } catch (err: any) {
    error("Setup failed: " + err.message);
    store.Actions.notify({
      text: "Setup failed!",
      source: "BB",
      type: ENotificationType.failure,
      toBeClosed: false,
    });
  } finally {
    if (interval) clearInterval(interval);
  }
  try {
    return { id: basename(_logFile), content: _decoder.decode(await readFile(_logFile)) }
  } catch (err: any) {
    return { id: basename(_logFile), content: `Failed to collect - ${err.message}!` }
  }
};

type AmiSetupOptions = {
  log: (msg: AmiLogMsg) => void;
  eventHook: (event: string) => void;
};

export const setup_ami = async (options: AmiSetupOptions) => {
  const _logFile = `/tmp/bb-ami-setup-${uuidv4()}.log`;
  const _decoder = new TextDecoder();
  let lastLogContent = "";
  let interval: NodeJS.Timeout | undefined = undefined;

  try {
    if (await exists(_logFile))
      lastLogContent = _decoder.decode(await readFile(_logFile));
    interval = setInterval(async () => {
      lastLogContent = await _read_log_file(_logFile, lastLogContent, options);
    }, 1000);
    await exec_sudo(
      `wget -q https://raw.githubusercontent.com/cryon-io/ami/master/install.sh -O /tmp/install.sh > ${_logFile} && sh /tmp/install.sh >> ${_logFile} 2>&1`
    );
    await _read_log_file(_logFile, lastLogContent, options);
    info("ami setup completed");
    store.Actions.notify({
      text: "Setup completed!",
      source: "BB",
      toBeClosed: true,
      type: ENotificationType.success,
    });
  } catch (err: any) {
    error("Setup failed: " + err.message);
  } finally {
    if (interval) clearInterval(interval);
  }
};

// ami --path=/bake-buddy/signer import-key --derivation-path=${options.derivPath} --force 								# user ?
// ami --path=/bake-buddy/signer ${LOCAL_SOURCES} client register key baker as delegate									# user ?
// ami --path=/bake-buddy/signer ${LOCAL_SOURCES} client setup ledger to bake for baker --main-chain-id NetXz969SFaFn8k --main-hwm ${options.blockNumber} 			# user ?
