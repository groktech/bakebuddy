export interface Report {
    id: string,
    content: string
}