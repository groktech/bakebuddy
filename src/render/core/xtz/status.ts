import { ref, Ref, computed } from "vue";
import HJson from "hjson";
import { exec } from "../ipc/proc";
import { error, info } from "@render/common/log";
import { EConfigurationOptions } from "@render/store/sub-stores/configuration/store/interfaces";
import store from "@render/store/store";
import { download_data } from "@render/core/ipc/net";
import { orderBy } from "lodash";

export const bakerInfo: Ref<any> = ref({});
export const signerInfo: Ref<any> = ref({});
export const isRefreshing: Ref<boolean> = ref(false);
export const timeout: Ref<NodeJS.Timeout | undefined> = ref();
export const infoUpdated: Ref<Date> = ref(new Date(0));
export const bakeRights: Ref<Array<Array<any>>> = ref([]);
export const endorsingRights: Ref<Array<Array<any>>> = ref([]);

const _collect_info = async () => {
  try {
    isRefreshing.value = true;
    await refresh_info();
    await refresh_baking_rights();
    await refresh_endorsing_rights()
  } finally {
    timeout.value = setTimeout(_collect_info, 30000);
    isRefreshing.value = false;
    infoUpdated.value = new Date();
  }
};

export const init_refresh = () => {
  if (timeout.value === undefined) {
    _collect_info();
  }
};

export const collect_app_info = async (appId: string) => {
  const _result = await exec(
    `ami --path="/bake-buddy/${appId}" --cache="/bake-buddy/cache" info`
  )
  const _appResult = HJson.parse(_result.stdout);
  return _appResult
}

export const refresh_info = async () => {
  try {
    console.log("refreshing");
    const _promises = {
      baker: collect_app_info("node"),
      signer: collect_app_info("signer"),
    };
    await Promise.allSettled(Object.values(_promises));
    try {
      bakerInfo.value = await _promises.baker;
    } catch (err: any) {
      console.error(err);
      error("Failed to get baker status: " + err.message);
    }
    try {
      signerInfo.value = await _promises.signer;
    } catch (err: any) {
      console.error(err);
      error("Failed to get signer status: " + err.message);
    }
  } catch (err: any) {
    console.error(err);
    error("Failed to collect bake buddy info: " + err.message);
  }
};

export const signerStatus = computed(() => {
  const _info = signerInfo.value;
  return {
    level: _info.level || "error",
    status: _info.status || "-",
    ledger: _info.ledger,
    service: {
      status: _info.signer || "-",
      started: _info.signer_started || "-",
    },
    baking_app: {
      version: _info.baking_app || "-",
      status: _info.baking_app_status || "not found",
    },
  };
});

export const bakerStatus = computed(() => {
  const _info = bakerInfo.value;
  return {
    level: _info.level || "error",
    status: _info.status || "-",
    synced: _info.synced || false,
    accuser: {
      status: _info.accuser || "-",
      started: _info.accuser_started || "-",
    },
    baker: {
      status: _info.baker || "-",
      started: _info.baker_started || "-",
    },
    endorser: {
      status: _info.endorser || "-",
      started: _info.endorser_started || "-",
    },
    node: {
      status: _info.node || "-",
      started: _info.node_started || "-",
    },
  };
});

export const nodeStatus = computed(() => {
  const _info = bakerInfo.value;
  return {
    connections: _info.connections,
    chain_head: _info.chain_head,
    bootstrapped: _info.bootstrapped,
    sync_state: _info.sync_state,
  };
});

const get_rights = async (feed: string, type: string, cycle: string) => {
  const _decoder = new TextDecoder();
  let _result: string | Uint8Array = "[]";
  if (!feed?.match("https?://") && cycle) {
    let _url = `https://api.tzkt.io/v1/rights?baker=${feed}&type=${type}&status=future&cycle=${cycle}`;
    if (type == "baking") _url = _url + `&priority=0`;
    _result = await download_data(_url);
  } else {
    const url = feed.replace("<cycle>", cycle);
    _result = await download_data(url);
  }
  return JSON.parse(
    typeof _result === "string" ? _result : _decoder.decode(_result)
  );
};

const get_baking_rights = async (feed: string, cycle: string) => {
  return await get_rights(feed, "baking", cycle);
};

const get_endorsing_rights = async (feed: string, cycle: string) => {
  return await get_rights(feed, "endorsing", cycle);
};

export const refresh_baking_rights = async () => {
  const bakeRightsFeed: string | undefined = store.Getters.Option(
    EConfigurationOptions.BakeRightsFeedSource
  );
  const cycle = bakerInfo.value?.chain_head?.cycle;
  try {
    if (bakeRightsFeed) {
      const bakingRights = await get_baking_rights(bakeRightsFeed, cycle);
      bakeRights.value = bakingRights;
    }
  } catch (err: any) {
    error(`Can not get baking rights - ${err.message}!`);
  }
};

export const refresh_endorsing_rights = async () => {
  const endorsingRightsFeed: string | undefined =
    store.Getters.Option(EConfigurationOptions.EndorsingRightsFeedSource) ||
    store.Getters.Option(EConfigurationOptions.BakeRightsFeedSource);
  const cycle = bakerInfo.value?.chain_head?.cycle;
  try {
    if (endorsingRightsFeed) {
      const _endorsingRights = await get_endorsing_rights(
        endorsingRightsFeed,
        cycle
      );
      endorsingRights.value = _endorsingRights;
    }
  } catch (err: any) {
    error(`Can not get endorsing rights - ${err.message}!`);
  }
};
