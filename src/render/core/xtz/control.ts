import { error, info } from "@render/common/log";
import store from "@render/store/store";
import { exists, readFile, writeFile } from "../ipc/fs";
import { exec_sudo } from "../ipc/proc";
import { v4 as uuidv4 } from "uuid";
import { AmiLogMsg, ENotificationType } from "@render/store/sub-stores/actions/store/interfaces";

type StartStopOptions = {
  log: (msg: AmiLogMsg) => void;
  eventHook: (event: string) => void;
};

const _decoder = new TextDecoder();
const _read_log_file = async (
  file: string,
  lastContent: string,
  options: StartStopOptions
) => {
  if (!(await exists(file))) return "";
  const log = _decoder.decode(await readFile(file));
  try {
    const newLog = log.replace(lastContent, "");
    const lines = newLog.split("\n");
    for (const line of lines) {
      if (!line) continue;
      const status = line.match(/### STATUS: (.*) ###/);
      if (status) {
        if (typeof options.eventHook === "function") {
          options.eventHook(status[1]);
        }
      } else {
        try {
          var logObj = JSON.parse(line.trim());
          options.log(logObj);
        } catch (err) {}
      }
    }
  } finally {
    return log;
  }
};

export const stop = async (options: StartStopOptions) => {
  const _logFile = `/tmp/bb-stop-${uuidv4()}.log`;

  let lastLogContent = "";
  let interval: NodeJS.Timeout | undefined = undefined;

  try {
    await writeFile(
      "/tmp/bb-stop.sh",
      `ami --path=/bake-buddy/node stop; ami --path=/bake-buddy/signer stop`
    );
    //await writeFile(_logFile, ""); // create empty file to keep user rights

    if (await exists(_logFile))
      lastLogContent = _decoder.decode(await readFile(_logFile));
    interval = setInterval(async () => {
      lastLogContent = await _read_log_file(_logFile, lastLogContent, options);
    }, 1000);
    await exec_sudo(`sh /tmp/bb-stop.sh > ${_logFile} 2>&1`);
    await _read_log_file(_logFile, lastLogContent, options);
    info("Stop completed");
    store.Actions.notify({
      text: "Stop completed!",
      type: ENotificationType.success,
      source: "BB",
    });
    return true;
  } catch (err: any) {
    error("Stop failed: " + err.message);
    store.Actions.notify({
      text: "Stop failed!",
      hint: err.message,
      source: "BB",
      type: ENotificationType.failure,
      toBeClosed: false,
    });
  } finally {
    if (interval) clearInterval(interval);
  }
  return false;
};

export const start = async (options: StartStopOptions) => {
  const _logFile = `/tmp/bb-start-${uuidv4()}.log`;
  const _decoder = new TextDecoder();
  let lastLogContent = "";
  let interval: NodeJS.Timeout | undefined = undefined;

  try {
    await writeFile(
      "/tmp/bb-start.sh",
      `ami --path=/bake-buddy/node start; ami --path=/bake-buddy/signer start`
    );
    //await writeFile(_logFile, ""); // create empty file to keep user rights
    if (await exists(_logFile))
      lastLogContent = _decoder.decode(await readFile(_logFile));
    interval = setInterval(async () => {
      lastLogContent = await _read_log_file(_logFile, lastLogContent, options);
    }, 1000);
    await exec_sudo(`sh /tmp/bb-start.sh > ${_logFile} 2>&1`);
    await _read_log_file(_logFile, lastLogContent, options);
    info("Start completed");
    store.Actions.notify({
      text: "Start completed!",
      source: "BB",
      type: ENotificationType.success,
    });
    return true;
  } catch (err: any) {
    error("Start failed: " + err.message);
    store.Actions.notify({
      text: "Start failed!",
      hint: err.message,
      source: "BB",
      type: ENotificationType.failure,
      toBeClosed: false,
    });
  } finally {
    if (interval) clearInterval(interval);
  }
  return false;
};

export const remove = async (options: StartStopOptions) => {
  const _logFile = `/tmp/bb-start-${uuidv4()}.log`;
  const _decoder = new TextDecoder();
  let lastLogContent = "";
  let interval: NodeJS.Timeout | undefined = undefined;

  try {
    await writeFile(
      "/tmp/bb-remove.sh",
      `ami --path=/bake-buddy/node stop; ami --path=/bake-buddy/node remove --all;
      ami --path=/bake-buddy/signer stop; ami --path=/bake-buddy/signer remove --all; 
      rm -rf /bake-buddy/node; rm -rf /bake-buddy/signer`
    );
    //await writeFile(_logFile, ""); // create empty file to keep user rights
    if (await exists(_logFile))
      lastLogContent = _decoder.decode(await readFile(_logFile));
    interval = setInterval(async () => {
      lastLogContent = await _read_log_file(_logFile, lastLogContent, options);
    }, 1000);
    await exec_sudo(`sh /tmp/bb-remove.sh > ${_logFile} 2>&1`);
    await _read_log_file(_logFile, lastLogContent, options);
    info("Remove completed");
    store.Actions.notify({
      text: "Remove completed!",
      source: "BB",
      type: ENotificationType.success,
    });
    return true;
  } catch (err: any) {
    error("Remove failed: " + err.message);
    store.Actions.notify({
      text: "Remove failed!",
      hint: err.message,
      source: "BB",
      type: ENotificationType.failure,
      toBeClosed: false,
    });
  } finally {
    if (interval) clearInterval(interval);
  }
  return false;
};