import { IExecOptions } from "@main/core/ipc/interfaces";
import { exists, writeFile } from "../ipc/fs";
import { spawn, exec } from "../ipc/proc";

const _execute = async (
  cmd: string,
  options: IExecOptions = {},
  _await: boolean = false
) => {
  const _decoder = new TextDecoder();
  const stdout = (data: Buffer | ArrayBuffer | Uint8Array) => {
    console.log(_decoder.decode(data));
  };
  const stderr = (data: Buffer | ArrayBuffer | Uint8Array) => {
    console.error(_decoder.decode(data));
  };

  writeFile(
    "/tmp/bb_cmd.sh",
    `printf 'sudo accepted\\n'; ${cmd.replace(/([$"])/g, "\\$1")}`
  );

  // TODO: Fix path

  await spawn(
    "/bin/sh",
    ["-c", cmd],
    {
      stdout: options?.stdout ?? stdout,
      stderr: options?.stderr ?? stderr,
      exit: options.exit
    },
    _await
  );
};

export const get_current_user = async () => {
  const _decoder = new TextDecoder();

  let _stdoutContent = "";
  await _execute(
    "whoami",
    {
      stdout: (data) => {
        _stdoutContent = _stdoutContent + _decoder.decode(data);
      }
    },
    true
  );
  return _stdoutContent.trim();
};
