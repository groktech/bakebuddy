export const usernameRules = [
    (v: string) => !!v || "Username cannot be empty",
    (v: string) => /^[a-z_][a-z0-9_]{0,30}$/.test(v) || "Please provide valid username"
];

export const appIdRules = usernameRules