//import { clipboard } from 'electron';
import store from '@render/store/store';
// TODO copy to clipboard through ipc
export const useCopyToClipboard = (messsage: string, notify: boolean = true, journal: boolean = false) => {
    const copyToClipboard = (data: string, notification?: { message: string, type: string, source?: string }) => {
        if (typeof notification === undefined) {
            notification = { message: messsage, type: "neutral", source: undefined, }
        }
        //clipboard.writeText(data);
        if (notify && notification?.message) {
            // TODO: use typed wrapper and journal option
            //store.dispatch("add_non_journaled_notification", notification);
        }
    };

    return {
        copyToClipboard
    }
}