import { Ref, ref } from 'vue';

export type ParameterDialogContext = {
    save?: (data: string) => void;
    close?: () => void;
    headline?: string,
    text: string,
    hint?: string,
    rules?: Array<(v: any) => boolean>
    active: boolean
};

export const useParameterDialog = () => {
    const defaultContext: ParameterDialogContext = { text: "", hint: "", active: false }
    const parameterDialogContext: Ref<ParameterDialogContext> = ref(defaultContext)
    const requestParameter = async (text: string, headline?: string, options?: any): Promise<boolean | string> => {
        return new Promise((resolve) => {
            parameterDialogContext.value = {
                text,
                headline: headline ?? "",
                ...(options ?? {}),
                save: (data) => {
                    parameterDialogContext.value = { ...parameterDialogContext.value, active: false }
                    resolve(data)
                },
                close: () => {
                    parameterDialogContext.value = { ...parameterDialogContext.value, active: false }
                    resolve(false)
                },
                active: true
            }
        })
    }
    return {
        parameterDialogContext,
        requestParameter
    }
}