import { Ref, ref } from 'vue';

export type ProgressDialogContext = {
    text: string,
    hint?: string,
    active: boolean
};

export const use_progress_dialog = () => {
    const defaultContext: ProgressDialogContext = { text: "", hint: "", active: false }
    const progressDialogContext: Ref<ProgressDialogContext> = ref(defaultContext)
    const show_progress = (text: string, hint?: string) => {
        progressDialogContext.value = {
            text,
            hint,
            active: true
        }
    }
    const hide_progress = () => {
        progressDialogContext.value = { ...progressDialogContext.value, active: false }
    }
    return {
        show_progress,
        hide_progress,
        progressDialogContext
    }
}