import { Ref, ref } from 'vue';

export enum EChoiceDialogContextOption {
    choose = "choose",
    cancel = "cancel",
    headline = "headline",
    options = "options",
    hint = "hint",
    active = "active",
    multi = "multi",
    text = "text",
    noOptions = "noOptions"
}
export type ChoiceDialogContextOptions<T> = {
    [EChoiceDialogContextOption.choose]?: (data: Array<T> | T) => void;
    [EChoiceDialogContextOption.cancel]?: () => void;
    [EChoiceDialogContextOption.headline]?: string,
    [EChoiceDialogContextOption.options]?: Array<T>,
    [EChoiceDialogContextOption.hint]?: string,
    [EChoiceDialogContextOption.multi]?: boolean,
    [EChoiceDialogContextOption.noOptions]?: string,
}
export type ChoiceDialogContext<T> = ChoiceDialogContextOptions<T> & {
    [EChoiceDialogContextOption.text]: string,
    [EChoiceDialogContextOption.active]: boolean
};

export const useChoiceDialog = () => {
    const defaultContext: ChoiceDialogContext<any> = { [EChoiceDialogContextOption.text]: "", hint: "", active: false }
    const choiceDialogContext: Ref<ChoiceDialogContext<any>> = ref(defaultContext)
    const requestChoice = async <T>(text: string, headline?: string, options?: ChoiceDialogContextOptions<T>): Promise<boolean | T[] | T> => {
        return new Promise((resolve) => {
            choiceDialogContext.value = {
                [EChoiceDialogContextOption.text]: text,
                headline: headline ?? "",
                ...(options ?? {}),
                choose: (chosen) => {
                    choiceDialogContext.value = { ...choiceDialogContext.value, active: false }
                    resolve(chosen)
                },
                cancel: () => {
                    choiceDialogContext.value = { ...choiceDialogContext.value, active: false }
                    resolve(false)
                },
                active: true
            }
        })
    }
    return {
        choiceDialogContext,
        requestChoice
    }
}