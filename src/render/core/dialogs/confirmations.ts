import { Ref, ref } from 'vue';

export type ConfirmDialogContext = {
    confirm?: () => void;
    deny?: () => void;
    headline?: string,
    question: string,
    hint: string,
    active: boolean
};

export const use_confirmation_dialog = () => {
    const defaultContext: ConfirmDialogContext  = { question: "", hint: "", active: false }
    const confirmationDialogContext: Ref<ConfirmDialogContext> = ref(defaultContext)
    const confirm = async (question: string, hint?: string): Promise<boolean> => {
        return new Promise((resolve) => {
            confirmationDialogContext.value = {
                question,
                hint: hint ?? "",
                confirm: () => {
                    confirmationDialogContext.value = { ...confirmationDialogContext.value, active: false }
                    resolve(true)
                },
                deny: () => {
                    confirmationDialogContext.value = { ...confirmationDialogContext.value, active: false }
                    resolve(false)
                },
                active: true
            }
        })
    }
    return {
        confirmationDialogContext,
        confirm
    }
}