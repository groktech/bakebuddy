export const download_file = async (sourceURL: string, targetPath: string): Promise<void> => {
    return await window.net.download_file(sourceURL, targetPath)
}

export const download_data = async (sourceURL: string): Promise<Uint8Array | string> => {
    return await window.net.download_data(sourceURL)
}

export const is_ip = async (ip: string): Promise<number> => {
    return await window.net.is_ip(ip)
}
