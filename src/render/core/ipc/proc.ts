import { IExecOptions } from "@main/core/ipc/interfaces"

export const exec = async (cmd: string, options?: any) => {
	return await window.proc.exec(cmd, options)
}

export const spawn = async (bin: string, args: Array<string>, _options: IExecOptions, _await: boolean) => {
	return await window.proc.spawn({ bin, args }, _options, _await)
}

export const exec_sudo = async (cmd: string) => {
	return await window.proc.sudo(cmd)
}