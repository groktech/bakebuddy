import { AtomicWritableOptions } from '@src/common/interfaces/fs'
import { WriteFileOptions } from 'fs'

export type TReadFileOptions = {
    encoding?: string;
    flag?: string;
}


// TODO: remove ignores
export const readFile = async (path: string, options?: TReadFileOptions): Promise<Uint8Array> => {
    return await window.fs.readFile(path, options)
}

export const readdir = async (path: string): Promise<Array<string>> => {
    return await window.fs.readdir(path)
}

export const mkdirp = async (path: string): Promise<void> => {
    return await window.fs.mkdirp(path)
}

export const realpath = (path: string, options?: { encoding: BufferEncoding }): Promise<string> => {
    return window.fs.realpath(path, options)
}

export const writeFile = async (path: string, data: any, options?: AtomicWritableOptions | string): Promise<void> => {
    await window.fs.writeFile(path, data, options)
}

export const rename = async (oldPath: string, newPath: string): Promise<void> => {  
    await window.fs.rename(oldPath, newPath)
}

export const exists = async (path: string): Promise<boolean> => {
    return await window.fs.exists(path)
}

export const statIsFile = async (path: string): Promise<boolean> => {
    return await window.fs.statIsFile(path)
}

export const statIsDirectory = async (path: string): Promise<boolean> => {
    return await window.fs.statIsDirectory(path)
}

export const sha256sum_file = async (path: string): Promise<string> => {
    return await window.fs.sha256sum_file(path)
}

export const sha256sum_data = async (data: Uint8Array): Promise<string> => {
    return await window.fs.sha256sum_data(data)
}