export const homedir = async (): Promise<string> => {
    return await window.os.homedir()
}

export const tmpdir = async (): Promise<string> => {
    return await window.os.tmpdir()
}

export const get_env = async(name: string) => {
    return await window.os.get_env(name)
}

export const cwd = async () => {
    return await window.os.cwd()
}

export const open = async (url: string) => {
    return await window.os.open(url)
}