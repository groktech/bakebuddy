export const write_to_clipboard = async (data:string): Promise<void> => {
    await window.clipboard.write(data)
}