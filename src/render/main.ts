//import '@src/common/patch'
import { createApp } from 'vue'
import App from './App.vue'
//import { ipcRenderer } from 'electron'
//import Store from 'electron-store'
import router from '@render/router'
import store from '@render/store/store'
import './styles/default.styl'

import { library } from '@fortawesome/fontawesome-svg-core'
import {
  faHome, faServer, faBookAlt, faCog, faSeedling, faKey,
  faPencilRuler, faInfo, faChevronLeft, faChevronRight, faRedo,
  faHorizontalRule, faEnvelope, faUserCircle, faArrowDown, faArrowUp,
  faCaretDown, faTimes, faTerminal, faEdit, faPencil, faSync, faTrashAlt,
  faEllipsisH, faChevronDown, faHistory, faPuzzlePiece, faPlus, faMapMarked, faFingerprint, 
  faExclamationTriangle, faMemory, faMicrochip, faHdd, faCalendarCheck, faPortalEnter, faDoorOpen, 
  faStream, faSparkles, faCloudDownload, faCloudUpload, faSyncAlt, faStarHalfAlt, faPlay, faStop, faComet, faHammer, faCube, faHashtag, faClock, faExclamation, faCheck, faPause,
  faBoxesAlt, faCircleNotch, faScroll, faScrollOld, faChartNetwork
} from '@fortawesome/pro-light-svg-icons'

import {
  faExclamationTriangle as faExclamationTriangleS, faTimesCircle as faTimesCircleS, faExclamationCircle as faExclamationCircleS, faExclamation as faExclamationS, faTimes as faTimesS
} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faHome, faServer, faBookAlt, faCog, faSeedling, faKey, faPencilRuler, faInfo,
  faChevronLeft, faChevronRight, faRedo, faHorizontalRule, faEnvelope, faUserCircle,
  faArrowDown, faArrowUp, faCaretDown, faTimes, faTerminal, faEdit, faPencil, faSync,
  faTrashAlt, faEllipsisH, faChevronDown, faHistory, faPuzzlePiece, faPlus, faMapMarked,
  faFingerprint, faExclamationTriangle, faMemory, faMicrochip, faHdd, faCalendarCheck,
  faDoorOpen, faStream, faSparkles, faCloudDownload, faCloudUpload, faSyncAlt, faStarHalfAlt, faPlay, faStop,
  faHammer, faCube, faHashtag, faClock, faExclamation, faCheck, faExclamationTriangleS, faTimesCircleS, faExclamationCircleS, faExclamationS, faTimesS, faPause,
  faBoxesAlt, faCircleNotch, faScrollOld, faScroll, faChartNetwork, )

createApp(App)
  .use(router)
  .use(store)
  .component('font-awesome-icon', FontAwesomeIcon)
  .mount('#app')
router.replace("/about")
  //.$nextTick(window.ClosePreloadLoading)
