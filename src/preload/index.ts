import { contextBridge, ipcRenderer } from 'electron'

import os from "./core/os"
import proc from "./core/proc"
import fs from "./core/fs"
import net from "./core/net"
import clipboard from "./core/clipboard"
import log from "./core/log"

contextBridge.exposeInMainWorld(
    'app',
    {
        loaded: () => ipcRenderer.send('app-loaded')
    }
)

contextBridge.exposeInMainWorld('clipboard', clipboard)
contextBridge.exposeInMainWorld('fs', fs)
contextBridge.exposeInMainWorld('net', net)
contextBridge.exposeInMainWorld('os', os)
contextBridge.exposeInMainWorld('proc', proc)
contextBridge.exposeInMainWorld('log', log)