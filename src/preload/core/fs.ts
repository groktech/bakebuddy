import { v4 as uuidv4 } from 'uuid'
import { ipcRenderer } from "electron"

export default {
    readFile: async (path, options) => {
        return new Promise((resolve, reject) => {
            const id = uuidv4()
            ipcRenderer.once(id, (event, { success, error, data }) => {
                if (!success)
                    reject(error)
                resolve(data)
            })
            ipcRenderer.send('readFile', id, { path, options })
        })
    },
    readdir: async (path) => {
        return new Promise((resolve, reject) => {
            const id = uuidv4()
            ipcRenderer.once(id, (event, { success, error, result }) => {
                if (!success)
                    reject(error)
                resolve(result)
            })
            ipcRenderer.send('readdir', id, { path })
        })
    },
    mkdirp: async (path) => {
        return new Promise((resolve, reject) => {
            const id = uuidv4()
            ipcRenderer.once(id, (event, { success, error }) => {
                if (!success)
                    reject(error)
                resolve()
            })
            ipcRenderer.send('mkdirp', id, { path })
        })
    },
    realpath: (path, options) => {
        return new Promise((resolve, reject) => {
            const id = uuidv4()
            ipcRenderer.once(id, (event, { success, result, error }) => {
                if (!success)
                    reject(error)
                resolve(result)
            })
            ipcRenderer.send('realpath', id, { path, options })
        })
    },
    writeFile: async (path, data, options) => {
        return new Promise((resolve, reject) => {
            const id = uuidv4()
            ipcRenderer.once(id, (event, { success, error }) => {
                if (!success)
                    reject(error)
                resolve()
            })
            ipcRenderer.send('writeFile', id, { path, data, options })
        })
    },
    rename: (oldPath, newPath) => {
        return new Promise((resolve, reject) => {
            const id = uuidv4()
            ipcRenderer.once(id, (event, { success, error }) => {
                if (!success)
                    reject(error)
                resolve()
            })
            ipcRenderer.send('rename', id, { oldPath, newPath })
        })
    },
    exists: (path) => {
        return new Promise((resolve, reject) => {
            const id = uuidv4()
            ipcRenderer.once(id, (event, { success }) => {
                resolve(success)
            })
            ipcRenderer.send('exists', id, { path })
        })
    },
    statIsFile: (path) => {
        return new Promise((resolve, reject) => {
            const id = uuidv4()
            ipcRenderer.once(id, (event, { success, error, result }) => {
                if (!success)
                    reject(error)
                resolve(result)
            })
            ipcRenderer.send('statIsFile', id, { path })
        })
    },
    statIsDirectory: (path) => {
        return new Promise((resolve, reject) => {
            const id = uuidv4()
            ipcRenderer.once(id, (event, { success, error, result }) => {
                if (!success)
                    reject(error)
                resolve(result)
            })
            ipcRenderer.send('statIsDirectory', id, { path })
        })
    },
    sha256sum_file: (path) => {
        return new Promise((resolve, reject) => {
            const id = uuidv4()
            ipcRenderer.once(id, (event, { success, error, result }) => {
                if (!success)
                    reject(error)
                resolve(result)
            })
            ipcRenderer.send('sha256sum_file', id, { path })
        })
    },
    sha256sum_data: (data) => {
        return new Promise((resolve, reject) => {
            const id = uuidv4()
            ipcRenderer.once(id, (event, { success, error, result }) => {
                if (!success)
                    reject(error)
                resolve(result)
            })
            ipcRenderer.send('sha256sum_data', id, { data })
        })
    }
} as IWindowFs