import { v4 as uuidv4 } from 'uuid'
import { ipcRenderer } from "electron"

export default {
    write: async (data: string) => {
        return new Promise((resolve, reject) => {
            const id = uuidv4()
            ipcRenderer.once(id, (event, { success, error }) => {
                if (!success)
                    reject(error)
                resolve()
            })
            ipcRenderer.send('write_to_clipboard', id, { data })
        })
    },
} as IWindowClipboard