import { v4 as uuidv4 } from 'uuid'
import { ipcRenderer } from "electron"

export default {
    homedir: async () => {
        return new Promise((resolve, reject) => {
            const id = uuidv4()
            ipcRenderer.once(id, (event, { success, error, result }) => {
                if (!success)
                    reject(error)
                resolve(result)
            })
            ipcRenderer.send('homedir', id)
        })
    },
    tmpdir: async () => {
        return new Promise((resolve, reject) => {
            const id = uuidv4()
            ipcRenderer.once(id, (event, { success, error, result }) => {
                if (!success)
                    reject(error)
                resolve(result)
            })
            ipcRenderer.send('tmpdir', id)
        })
    },
    get_env: (name: string) => {
        return new Promise((resolve, reject) => {
            const id = uuidv4()
            ipcRenderer.once(id, (event, { success, error, result }) => {
                if (!success)
                    reject(error)
                resolve(result)
            })
            ipcRenderer.send('get_env', id, { name })
        })
    },
    cwd: () => {
        return new Promise((resolve, reject) => {
            const id = uuidv4()
            ipcRenderer.once(id, (event, { success, error, result }) => {
                if (!success)
                    reject(error)
                resolve(result)
            })
            ipcRenderer.send('cwd', id)
        })
    },
    open: (url: string) => {
        return new Promise((resolve, reject) => {
            const id = uuidv4()
            ipcRenderer.once(id, (event, { success, error }) => {
                if (!success)
                    reject(error)
                resolve()
            })
            ipcRenderer.send('open', id, { url })
        })
    }
} as IWindowOs