import { v4 as uuidv4 } from 'uuid'
import { ipcRenderer, IpcRendererEvent } from "electron"
import { IExecOptions } from '@main/core/ipc/interfaces'
import { IWindowProc } from '@render/typings'

export default {
	exec: (cmd: string, options: any) => {
		return new Promise((resolve, reject) => {
			const id = uuidv4()
			ipcRenderer.once(id, (event, { success, error, result }) => {
				if (!success)
					reject(error)
				resolve(result)
			})
			ipcRenderer.send('exec', id, { cmd, options })
		})
	},
	spawn: async ({ bin, args }: { bin: string, args: Array<string> }, _options: IExecOptions, _await: boolean) => {
		return new Promise((resolve, reject) => {
			const cmdId = uuidv4()
			ipcRenderer.once(cmdId, (event, { success, error }) => {
				if (!success)
					reject(error)

				if (!_await)
					resolve(cmdId)
			})
			if (_options && typeof _options.stdout === 'function') {
				const _stdoutCb = _options.stdout

				const stdout = (event: IpcRendererEvent, data: ArrayBuffer | Uint8Array) => {
					_stdoutCb(data)
				}

				ipcRenderer.on(`${cmdId}_stdout`, stdout)
				ipcRenderer.once(`${cmdId}_close`,
					() => ipcRenderer.removeListener(`${cmdId}_stdout`, stdout))
			}
			if (_options && typeof _options.stderr === 'function') {
				const stderrCb = _options.stderr

				const stderr = (event: IpcRendererEvent, data: ArrayBuffer | Uint8Array) => {
					stderrCb(data)
				}

				ipcRenderer.on(`${cmdId}_stderr`, stderr)
				ipcRenderer.once(`${cmdId}_close`,
					() => ipcRenderer.removeListener(`${cmdId}_stderr`, stderr))
			}

			ipcRenderer.once(`${cmdId}_exit`, (event, code) => {
				if (_options && typeof _options.exit === 'function') {
					_options.exit(code)
				}
				if (_await) {
					resolve(code)
				}
			})
			ipcRenderer.send('spawn', cmdId, {
				bin, args
			})
		})
	},
	sudo: (cmd: string) => {
		return new Promise((resolve, reject) => {
			const id = uuidv4()
			ipcRenderer.once(id, (event, { success, error, stdout, stderr }) => {
				if (!success)
					reject(error)
				resolve({ stdout, stderr })
			})
			ipcRenderer.send('sudo', id, cmd)
		})
	}
} as IWindowProc