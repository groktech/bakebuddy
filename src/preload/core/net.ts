import { v4 as uuidv4 } from 'uuid'
import { ipcRenderer } from "electron"

export default {
    download_file: async (sourceURL, targetPath) => {
        return new Promise((resolve, reject) => {
            const id = uuidv4()
            ipcRenderer.once(id, (event, { success, error }) => {
                if (!success)
                    reject(error)
                resolve()
            })
            ipcRenderer.send('download_file', id, { sourceURL, targetPath })
        })
    },
    download_data: async (sourceURL) => {
        return new Promise((resolve, reject) => {
            const id = uuidv4()
            ipcRenderer.once(id, (event, { success, error, data }) => {
                if (!success)
                    reject(error)
                resolve(data)
            })
            ipcRenderer.send('download_string', id, { sourceURL })
        })
    },
    is_ip: async (ip) => {
        return new Promise((resolve, reject) => {
            const id = uuidv4()
            ipcRenderer.once(id, (event, { success, error, result }) => {
                if (!success)
                    reject(error)
                resolve(result)
            })
            ipcRenderer.send('is_ip', id, { ip })
        })
    }
} as IWindowNet