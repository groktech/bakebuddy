import { v4 as uuidv4 } from 'uuid'
import { ipcRenderer } from "electron"

const create_ipc_log_method = (target: string) => {
	return async (msg: string | object): Promise<void> => {
        return new Promise((resolve, reject) => {
            const id = uuidv4()
            ipcRenderer.once(id, (event, { success, error, result }) => {
                if (!success)
                    reject(error)
                resolve()
            })
            ipcRenderer.send(target, id, msg)
        })
    }
}

export default {
    trace: create_ipc_log_method("trace"),
	debug: create_ipc_log_method("debug"),
	info: create_ipc_log_method("info"),
	warn: create_ipc_log_method("warn"),
	error: create_ipc_log_method("error"),
	fatal: create_ipc_log_method("fatal"),
} as IWindowLog