
import * as fs from 'fs';
import { mkdir, writeFile } from "fs/promises"
import * as path from 'path';
import { exec } from 'child_process';
import { promisify } from 'util';

import { Plugin } from 'rollup';

import { RollupAutoInstallOptions } from './auto-install-types';
import { chdir, cwd } from 'process';

const execAsync = promisify(exec);

export default function autoInstall(opts: RollupAutoInstallOptions = {}): Plugin {
	const defaults = {
		// intentionally undocumented options. used for tests
		commands: {
			npm: 'npm install',
			yarn: 'yarn install'
		},
		manager: fs.existsSync('yarn.lock') ? 'yarn' : 'npm',
		pkgFile: path.resolve(opts.pkgFile || 'package.json')
	};

	const argMap = {
		npm: {
			ignoreOptional: '--no-optional',
			production: '--only=prod'
		},
		yarn: {
			ignoreOptional: "--ignore-optional",
			production: '--production=true'
		}
	}

	const options = Object.assign({}, defaults, opts);
	const { manager, pkgFile, destination, dependencies, ignoreOptional, production } = options;
	const validManagers = ['npm', 'yarn'];

	if (!validManagers.includes(manager)) {
		throw new RangeError(
			`'${manager}' is not a valid package manager. ` +
			`Valid managers include: '${validManagers.join("', '")}'.`
		);
	}

	let pkg: any;
	let pkgFound = false
	if (fs.existsSync(pkgFile)) {
		pkgFound = true;
		pkg = JSON.parse(fs.readFileSync(pkgFile, 'utf-8'));
	} else {
		fs.writeFileSync(pkgFile, '{}');
		pkg = {};
	}

	const cmd = options.commands[manager];


	return {
		name: 'install-deps-into',
		async writeBundle(options) {
			const outputDestination = destination ?? options.dir
			if (outputDestination) {
				await mkdir(outputDestination, { recursive: true });
				const packageJson = { 
					version: pkg.version,
					name: pkg.name,
					main: pkg.main,
					description: pkg.description,
					author: pkg.author,
					dependencies: pkg.dependencies, 
				}
				if (Array.isArray(dependencies)) {
					const deps: { [key: string]: string } = {}
					for (const dep of Object.keys(pkg.dependencies)) {
						if (dependencies.includes(dep)) {
							deps[dep] = pkg.dependencies[dep]
						}
					}
					packageJson.dependencies = deps
				}
				await writeFile(path.join(outputDestination, "package.json"), JSON.stringify(packageJson));
			}

			if (!outputDestination) {
				throw new Error("Destination was not specified and can not be determined from output options!")
			}

			console.log(`Installing dependencies into ${outputDestination}`)
			const currentDir = cwd()
			chdir(outputDestination)
			const args = [ '--non-interactive' ]
			if (ignoreOptional) {
				args.push(argMap[manager].ignoreOptional)
			}
			if (production !== false) {
				args.push(argMap[manager].production)
			}
			await execAsync(`${cmd} ${args.join(' ')}`);
			chdir(currentDir)
		}
	};
}