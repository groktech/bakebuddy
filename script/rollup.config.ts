import { join } from 'path'
import { RollupOptions } from 'rollup'
import nodeResolve from '@rollup/plugin-node-resolve'
import commonjs from '@rollup/plugin-commonjs'
import typescript from '@rollup/plugin-typescript'
import alias from '@rollup/plugin-alias'
import json from '@rollup/plugin-json'
import { builtins } from './utils'
import auto from './auto-install-plugin';
import copy from 'rollup-plugin-copy'
import minimist from 'minimist'
const { uglify } = require("rollup-plugin-uglify")
import obfuscator from 'rollup-plugin-obfuscator';

const argv = minimist(process.argv.slice(2))
const production = !argv.watch;

export default (env = 'production') => {
	const mainOptions: RollupOptions = {
		input: join(__dirname, '../src/main/index.ts'),
		output: {
			dir: join(__dirname, '../dist/main/'),
			format: 'cjs',
			name: 'ElectronMainBundle',
			sourcemap: !production,
		},
		plugins: [
			auto({
				dependencies: [
					"ssh2",
					"jszip",
					"rss-parser"
				],
				pkgFile: join(__dirname, '../package.json'),
				destination: join(__dirname, '../dist/'),
				ignoreOptional: true
			}),
			nodeResolve({ browser: false, preferBuiltins: true, dedupe: ["ssh2"] }),
			commonjs({
				requireReturnsDefault: true,
				ignoreTryCatch: (module) => {
					const ignore = [
						"pino-pretty",
					]

					if (ignore.includes(module)) {
						return true
					}
					return false
				},
				exclude: [
					'electron-devtools-installer'
				]
			}),
			json(),
			typescript({
				module: 'ESNext'
			}),
			//nodePolyfills(),
			alias({
				entries: [
					{ find: '@render', replacement: join(__dirname, '../src/render') },
					{ find: '@main', replacement: join(__dirname, '../src/main') },
					{ find: '@preload', replacement: join(__dirname, '../src/preload') },
					{ find: '@src', replacement: join(__dirname, '../src') },
					{ find: '@root', replacement: join(__dirname, '..') },
				]
			}),
			copy({
				// targets: [
				// 	{
				// 		src: 'src/main/assets/icon.png',
				// 		dest: 'dist/'
				// 	},
				// ],
				// flatten: true
			}),
			production && uglify({
				compress: {
					drop_console: true,
					keep_infinity: true
				},
				toplevel: true
			})
		],
		external: [
			...builtins(),
			'electron',
			'fs/promises',
			'electron-devtools-installer',
			'ssh2',
			'jszip',
			'xmlbuilder'
		],
	}

	const preloadOptions: RollupOptions = {
		input: join(__dirname, '../src/preload/index.ts'),
		output: {
			dir: join(__dirname, '../dist/preload/'),
			format: 'cjs',
			name: 'ElectronMainBundle',
			sourcemap: !production,
		},
		plugins: [
			nodeResolve(),
			commonjs({
				include: './node_modules/**',
			}),
			json(),
			typescript({
				module: 'ESNext',
			}),
			alias({
				entries: [
					{ find: '@render', replacement: join(__dirname, '../src/render') },
					{ find: '@main', replacement: join(__dirname, '../src/main') },
					{ find: '@preload', replacement: join(__dirname, '../src/preload') },
					{ find: '@src', replacement: join(__dirname, '../src') },
					{ find: '@root', replacement: join(__dirname, '..') },
				]
			})
		],
		external: [
			...builtins(),
			'electron'
		],
	}

	return [mainOptions, preloadOptions]
}
