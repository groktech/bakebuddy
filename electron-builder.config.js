module.exports = {
	asar: true,
	npmRebuild: false,
	buildDependenciesFromSource: true,
	copyright: "Copyright © 2021 bakebuddy.xyz & alis.is",
	productName: "Bake Buddy",
	remoteBuild: false,
	directories: {
		"output": "release/${version}",
		"app": "dist"
	},
	files: [
		"**"
	],
	win: {
		"icon": "build/icon.png",
		"target": [
			{
				"target": "nsis",
				"arch": [
					"x64"
				]
			},
			{
				"target": "zip",
				"arch": [ "x64" ]
			}
		],
		"artifactName": "${productName}_${version}.${ext}"
	},
	linux: {
		"icon": "build/icon.png",
		target: "AppImage"
	},
	nsis: {
		"oneClick": false,
		"perMachine": false,
		"allowToChangeInstallationDirectory": true,
		"deleteAppDataOnUninstall": false,
		installerIcon: "build/icon.ico",
	}
}