require('dotenv').config({ path: join(__dirname, '.env') })

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { join } from 'path'
import minimist from 'minimist'

const root = join(__dirname, 'src/render')

const argv = minimist(process.argv.slice(3))
const production = !argv.development

export default defineConfig(env => {
	return {
		plugins: [
			vue()
		],
		root,
		base: production ? '/' : './',
		server: {
			port: +process.env.PORT,
		},
		resolve: {
			alias: {
				'@render': join(__dirname, 'src/render'),
				'@main': join(__dirname, 'src/main'),
				'@preload': join(__dirname, 'src/preload'),
				'@src': join(__dirname, 'src'),
				'@root': __dirname,
			},
		},
		build: {
			outDir: join(__dirname, 'dist/render'),
			emptyOutDir: true,
			minify: production,
			commonjsOptions: {},
			assetsDir: '', // Relative path loading problem
			sourcemap: !production,
			rollupOptions: {
				plugins: []
			}
		},
	}
})
